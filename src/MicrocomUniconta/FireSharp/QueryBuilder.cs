﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MicrocomUniconta.FireSharp
{
    public class QueryBuilder
    {
        private readonly string _initialQuery;
        private readonly string _formatParam = "format";
        private readonly string _shallowParam = "shallow";
        private readonly string _orderByParam = "orderBy";
        private readonly string _startAtParam = "startAt";
        private readonly string _endAtParam = "endAt";
        private readonly string _eqaulToParam = "equalTo";
        private readonly string _formatVal = "export";
        private readonly string _limitToFirstParam = "limitToFirst";
        private readonly string _limitToLastParam = "limitToLast";
        private readonly string _printParam = "print";

        static Dictionary<string, object> _query = new Dictionary<string, object>();

        private QueryBuilder(string initialQuery = "")
        {
            _initialQuery = initialQuery;
            _query = new Dictionary<string, object>();
        }

        public static QueryBuilder New(string initialQuery = "")
        {
            return new QueryBuilder(initialQuery);
        }
        public QueryBuilder StartAt(string value)
        {
            return AddToQueryDictionary(_startAtParam, value);
        }

        public QueryBuilder StartAt(long value)
        {
            return AddToQueryDictionary(_startAtParam, value);
        }

        public QueryBuilder EndAt(string value)
        {
            return AddToQueryDictionary(_endAtParam, value);
        }

        public QueryBuilder EndAt(long value)
        {
            return AddToQueryDictionary(_endAtParam, value);
        }

        public QueryBuilder EqualTo(string value)
        {
            return AddToQueryDictionary(_eqaulToParam, value);
        }

        public QueryBuilder OrderBy(string value)
        {
            return AddToQueryDictionary(_orderByParam, value);
        }

        public QueryBuilder LimitToFirst(int value)
        {
            return AddToQueryDictionary(_limitToFirstParam, value > 0 ? value.ToString() : string.Empty, skipEncoding: true);
        }

        public QueryBuilder LimitToLast(int value)
        {
            return AddToQueryDictionary(_limitToLastParam, value > 0 ? value.ToString() : string.Empty, skipEncoding: true);
        }



        public QueryBuilder Shallow(bool value)
        {
            return AddToQueryDictionary(_shallowParam, value ? "true" : string.Empty, skipEncoding: true);
        }

        public QueryBuilder Print(string value)
        {
            return AddToQueryDictionary(_printParam, value, skipEncoding: true);
        }

        public QueryBuilder IncludePriority(bool value)
        {
            return AddToQueryDictionary(_formatParam, value ? _formatVal : string.Empty, skipEncoding: true);
        }

        private QueryBuilder AddToQueryDictionary(string parameterName, string value, bool skipEncoding = false)
        {
            if (!string.IsNullOrEmpty(value))
            {
                _query.Add(parameterName, skipEncoding ? value : EscapeString(value));
            }
            else
            {
                _query.Remove(_startAtParam);
            }

            return this;
        }

        private QueryBuilder AddToQueryDictionary(string parameterName, long value)
        {
            _query.Add(parameterName, value);
            return this;
        }

        private string EscapeString(string value)
        {
            return $"\"{Uri.EscapeDataString(value).Replace("%20", "+").Trim('\"')}\"";
        }

        public string ToQueryString()
        {
            if (!_query.Any() && !string.IsNullOrEmpty(_initialQuery)) return _initialQuery;

            return !string.IsNullOrEmpty(_initialQuery)
                ? $"{_initialQuery}&{string.Join("&", _query.Select(pair => $"{pair.Key}={pair.Value}").ToArray())}"
                : string.Join("&", _query.Select(pair => $"{pair.Key}={pair.Value}").ToArray());
        }
    }
}
