using System;

namespace MicrocomUniconta.FireSharp.EventStreaming
{
    public class ValueRemovedEventArgs : EventArgs
    {
        public ValueRemovedEventArgs(string path)
        {
            Path = path;
        }

        public string Path { get; private set; }
    }
}