﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicrocomUniconta.Serialnumber.API;
using MicrocomUniconta.Serialnumber.Views;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.SerialNumber
{
    public class Show_OrderLine_Serialnumbers : IPluginBase
    {
        private SerialNumberUnicontaAPIHelpers _api;

        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "vis_debitor_serienumre";

        public event EventHandler OnExecute;

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {
            DebtorOrderLineClient debtorOrderLine = (DebtorOrderLineClient)currentRow;

            _api = new SerialNumberUnicontaAPIHelpers(_crudApi);

            var task = Task.Run(async () => await _api.SearchSerialNumbersAsync(debtorOrderLine.Item));
            task.Wait();
            var serialNumbers = task.Result;
            
            var showForm = new ListSalesOrderLineSerialNumbersForm(_crudApi, serialNumbers, debtorOrderLine);
            if (showForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                showForm.Dispose();
                return ErrorCodes.Succes;
            }
            else
            {
                showForm.Dispose();
                return ErrorCodes.Succes;
            }
        }      

        #region Uniconta Setup

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {

        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters)
        {
        }

        #endregion
    
    }
}
