﻿using MicrocomUniconta.SerialNumber.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.Serialnumber.API
{
    public class InvItemUnicontaAPIHelpers
    {
        private readonly CrudAPI _crudApi;

        public InvItemUnicontaAPIHelpers(CrudAPI crudApi)
        {
            _crudApi = crudApi;
        }

        public async Task<InvItemClient> GetItem(string invItem)
        {
            var criteria = new List<PropValuePair>();
            var whereClause = PropValuePair.GenereteWhereElements("Item", typeof(string), invItem);
            criteria.Add(whereClause);
            var output = await _crudApi.Query(new InvItemClient(), null, criteria);
            return output[0];
        }    
        
        public async Task<IEnumerable<InvItemClient>> GetAllItem()
        {
            var output = await _crudApi.Query(new InvItemClient(), null, null);
            return output;
        }
    }
}
