﻿using MicrocomUniconta.SerialNumber.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.Serialnumber.API
{
    public class SerialNumberUnicontaAPIHelpers
    {
        private readonly CrudAPI _crudApi;

        public SerialNumberUnicontaAPIHelpers(CrudAPI crudApi)
        {
            _crudApi = crudApi;
        }

        public async Task<CreditorClient> GetVendorWhereAccountAsync(string account)
        {
            var criteria = new List<PropValuePair>();
            var whereClause = PropValuePair.GenereteWhereElements("Account", typeof(string), account);
            criteria.Add(whereClause);

            var output = await _crudApi.Query(new CreditorClient(), null, criteria);
            return output[0];
        }

        public async Task<TblSerienummer[]> GetSerialnumbersByInvItemVendorAccountAsync(string invItem, string vendorAccount)
        {
            var criteria = new List<PropValuePair>();
            var whereClause = PropValuePair.GenereteWhereElements("Varenummer", typeof(string), invItem);
            var whereClause2 = PropValuePair.GenereteWhereElements("Kreditor", typeof(string), vendorAccount);
            criteria.Add(whereClause);
            criteria.Add(whereClause2);

            var output = await _crudApi.Query(new TblSerienummer(), null, criteria);
            return output;
        }

        public async Task<TblSerienummer[]> GetSerialnumbersWhereInvItemAsync(string invItem)
        {
            var criteria = new List<PropValuePair>();
            var whereClause = PropValuePair.GenereteWhereElements("Varenummer", typeof(string), invItem);
            criteria.Add(whereClause);

            var output = await _crudApi.Query(new TblSerienummer(), null, criteria);
            return output;
        }

        public async Task<TblSerienummer[]> SearchSerialNumbersAsync(string invItem, string vendorAccount, string serialNumber)
        {
            var criteria = new List<PropValuePair>();
            if (!string.IsNullOrEmpty(invItem))
            {
                var whereClause = PropValuePair.GenereteWhereElements("Varenummer", typeof(string), invItem);
                criteria.Add(whereClause);
            }
            if (!string.IsNullOrEmpty(vendorAccount))
            {
                var whereClause = PropValuePair.GenereteWhereElements("Kreditor", typeof(string), vendorAccount);
                criteria.Add(whereClause);
            }
            if (!string.IsNullOrEmpty(serialNumber))
            {
                var whereClause = PropValuePair.GenereteWhereElements("Serienummer", typeof(string), serialNumber);
                criteria.Add(whereClause);
            }

            var output = await _crudApi.Query(new TblSerienummer(), null, criteria);
            return output;
        }

        public async Task<TblSerienummer[]> GetAllSerialNumbers()
        {         
            var output = await _crudApi.Query(new TblSerienummer(), null, null);
            return output;
        }

        public async Task<TblSerienummer[]> SearchSerialNumbersAsync(string invItem)
        {
            var criteria = new List<PropValuePair>();            
            var invItemClause = PropValuePair.GenereteWhereElements("Varenummer", typeof(string), invItem);
            criteria.Add(invItemClause);
            var orderBy = PropValuePair.GenereteOrderByElement("Debitor", false);
            criteria.Add(orderBy);
            
            var output = await _crudApi.Query(new TblSerienummer(), null, criteria);
            return output;
        }

        public async Task<TblSerienummer[]> GetSerialNumbersWhereDebitorAsync(string account)
        {
            var criteria = new List<PropValuePair>();
            var whereClause = PropValuePair.GenereteWhereElements("Debitor", typeof(string), account);
            criteria.Add(whereClause);

            var output = await _crudApi.Query(new TblSerienummer(), null, criteria);
            return output;
        }

        public async Task<DebtorClient[]> GetAllDebtorsAsync()
        {
            return await _crudApi.Query(new DebtorClient(), null, null);
        }

        public async Task<CreditorClient[]> GetAllVendorsAsync()
        {
            return await _crudApi.Query(new CreditorClient(), null, null);
        }

        public async Task<InvItemClient[]> GetAllItemsAsync()
        {
            return await _crudApi.Query(new InvItemClient(), null, null);
        }

        public async Task<ErrorCodes> InsertSerialNumbersAsync(List<TblSerienummer> serialNumbers)
        {

            try
            {
                var output = await _crudApi.Insert(serialNumbers);
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public async Task<ErrorCodes> UpdateSerialNumberAsync(TblSerienummer tblSerienummer)
        {
            return await _crudApi.Update(tblSerienummer);
        }

        public async Task<ErrorCodes> DeleteSerialNumberAsync(TblSerienummer tblSerienummer)
        {
            return await _crudApi.Delete(tblSerienummer);
        }

        public async Task<ErrorCodes> DeleteSerialNumbersAsync(List<TblSerienummer> tblSerienummers)
        {
            return await _crudApi.Delete(tblSerienummers);
        }
    }
}
