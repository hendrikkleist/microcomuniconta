﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MicrocomUniconta.Serialnumber.API;
using MicrocomUniconta.Serialnumber.Views;
using MicrocomUniconta.SerialNumber.Models;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.SerialNumber
{
    public class Show_Item_Serialnumbers : IPluginBase
    {
        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "vis_debitor_serienumre";

        public event EventHandler OnExecute;

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {
            InvItemClient invItem = (InvItemClient)currentRow;
            var api = new SerialNumberUnicontaAPIHelpers(_crudApi);
            var task = Task.Run(async () => await api.GetSerialnumbersWhereInvItemAsync(invItem.Item));
            task.Wait();
            var serialNumbers = task.Result;
            
            var showForm = new ListItemSerialNumbersForm(_crudApi, serialNumbers, invItem);
            if (showForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                showForm.Dispose();
                return ErrorCodes.Succes;
            }
            else
            {
                showForm.Dispose();
                return ErrorCodes.Succes;
            }
        }

        #region Uniconta Setup

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {

        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters)
        {
        }

        #endregion
    }
}
