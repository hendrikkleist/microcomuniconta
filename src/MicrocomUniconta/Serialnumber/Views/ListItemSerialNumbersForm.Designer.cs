﻿
namespace MicrocomUniconta.Serialnumber.Views
{
    partial class ListItemSerialNumbersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GboxTitle = new System.Windows.Forms.GroupBox();
            this.LblInvItem = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnSave = new System.Windows.Forms.Button();
            this.DtpClosingDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DtpSaleDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.CbDebtor = new System.Windows.Forms.ComboBox();
            this.TxtRemark = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DtpBuyDate = new System.Windows.Forms.DateTimePicker();
            this.CbxClosed = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GboxTitle
            // 
            this.GboxTitle.Location = new System.Drawing.Point(14, 35);
            this.GboxTitle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GboxTitle.Name = "GboxTitle";
            this.GboxTitle.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GboxTitle.Size = new System.Drawing.Size(1233, 594);
            this.GboxTitle.TabIndex = 1;
            this.GboxTitle.TabStop = false;
            this.GboxTitle.Text = "Data";
            // 
            // LblInvItem
            // 
            this.LblInvItem.AutoSize = true;
            this.LblInvItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.LblInvItem.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LblInvItem.Location = new System.Drawing.Point(0, 0);
            this.LblInvItem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblInvItem.Name = "LblInvItem";
            this.LblInvItem.Padding = new System.Windows.Forms.Padding(0, 9, 0, 0);
            this.LblInvItem.Size = new System.Drawing.Size(43, 29);
            this.LblInvItem.TabIndex = 11;
            this.LblInvItem.Text = "Vare";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnSave);
            this.panel1.Controls.Add(this.DtpClosingDate);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.DtpSaleDate);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.CbDebtor);
            this.panel1.Controls.Add(this.TxtRemark);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.DtpBuyDate);
            this.panel1.Controls.Add(this.CbxClosed);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(14, 639);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1234, 168);
            this.panel1.TabIndex = 15;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(969, 101);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(141, 35);
            this.BtnSave.TabIndex = 20;
            this.BtnSave.Text = "&Gem";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // DtpClosingDate
            // 
            this.DtpClosingDate.Location = new System.Drawing.Point(9, 103);
            this.DtpClosingDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DtpClosingDate.Name = "DtpClosingDate";
            this.DtpClosingDate.Size = new System.Drawing.Size(338, 26);
            this.DtpClosingDate.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 75);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 20);
            this.label7.TabIndex = 25;
            this.label7.Text = "Lukkedato";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(693, 14);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 20);
            this.label9.TabIndex = 24;
            this.label9.Text = "Salgsdato";
            // 
            // DtpSaleDate
            // 
            this.DtpSaleDate.Location = new System.Drawing.Point(697, 44);
            this.DtpSaleDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DtpSaleDate.Name = "DtpSaleDate";
            this.DtpSaleDate.Size = new System.Drawing.Size(334, 26);
            this.DtpSaleDate.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 20);
            this.label2.TabIndex = 21;
            this.label2.Text = "Debitor";
            // 
            // CbDebtor
            // 
            this.CbDebtor.FormattingEnabled = true;
            this.CbDebtor.Location = new System.Drawing.Point(9, 42);
            this.CbDebtor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CbDebtor.Name = "CbDebtor";
            this.CbDebtor.Size = new System.Drawing.Size(338, 28);
            this.CbDebtor.TabIndex = 14;
            // 
            // TxtRemark
            // 
            this.TxtRemark.Location = new System.Drawing.Point(360, 103);
            this.TxtRemark.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TxtRemark.Name = "TxtRemark";
            this.TxtRemark.Size = new System.Drawing.Size(338, 26);
            this.TxtRemark.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(355, 75);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 20);
            this.label8.TabIndex = 20;
            this.label8.Text = "Bemærkning";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(351, 14);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 20);
            this.label5.TabIndex = 17;
            this.label5.Text = "Købsdato";
            // 
            // DtpBuyDate
            // 
            this.DtpBuyDate.Location = new System.Drawing.Point(355, 44);
            this.DtpBuyDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DtpBuyDate.Name = "DtpBuyDate";
            this.DtpBuyDate.Size = new System.Drawing.Size(334, 26);
            this.DtpBuyDate.TabIndex = 8;
            // 
            // CbxClosed
            // 
            this.CbxClosed.AutoSize = true;
            this.CbxClosed.Location = new System.Drawing.Point(1056, 43);
            this.CbxClosed.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CbxClosed.Name = "CbxClosed";
            this.CbxClosed.Size = new System.Drawing.Size(58, 24);
            this.CbxClosed.TabIndex = 12;
            this.CbxClosed.Text = "Nej";
            this.CbxClosed.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1053, 14);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 20);
            this.label6.TabIndex = 15;
            this.label6.Text = "Lukket";
            // 
            // ListItemSerialNumbersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 1187);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LblInvItem);
            this.Controls.Add(this.GboxTitle);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ListItemSerialNumbersForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Title";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GboxTitle;
        private System.Windows.Forms.Label LblInvItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.DateTimePicker DtpClosingDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker DtpSaleDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CbDebtor;
        private System.Windows.Forms.TextBox TxtRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker DtpBuyDate;
        private System.Windows.Forms.CheckBox CbxClosed;
        private System.Windows.Forms.Label label6;
    }
}