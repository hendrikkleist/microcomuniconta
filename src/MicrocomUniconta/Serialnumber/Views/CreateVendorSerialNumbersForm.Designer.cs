﻿
namespace MicrocomUniconta.Serialnumber.Views
{
    partial class CreateVendorSerialNumbersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GboxTitle = new System.Windows.Forms.GroupBox();
            this.LblItemNo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.DtpClosingDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DtpSaleDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.CbDebtor = new System.Windows.Forms.ComboBox();
            this.TxtRemark = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DtpBuyDate = new System.Windows.Forms.DateTimePicker();
            this.CbxClosed = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtSerialNumber = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GboxTitle
            // 
            this.GboxTitle.Location = new System.Drawing.Point(12, 25);
            this.GboxTitle.Name = "GboxTitle";
            this.GboxTitle.Size = new System.Drawing.Size(819, 331);
            this.GboxTitle.TabIndex = 1;
            this.GboxTitle.TabStop = false;
            this.GboxTitle.Text = "Data";
            // 
            // LblItemNo
            // 
            this.LblItemNo.AutoSize = true;
            this.LblItemNo.Location = new System.Drawing.Point(9, 9);
            this.LblItemNo.Name = "LblItemNo";
            this.LblItemNo.Size = new System.Drawing.Size(66, 13);
            this.LblItemNo.TabIndex = 11;
            this.LblItemNo.Text = "Varenummer";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnDelete);
            this.panel1.Controls.Add(this.BtnSave);
            this.panel1.Controls.Add(this.DtpClosingDate);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.DtpSaleDate);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.CbDebtor);
            this.panel1.Controls.Add(this.TxtRemark);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.DtpBuyDate);
            this.panel1.Controls.Add(this.CbxClosed);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.TxtSerialNumber);
            this.panel1.Location = new System.Drawing.Point(12, 362);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(819, 133);
            this.panel1.TabIndex = 13;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(106, 95);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(94, 23);
            this.BtnDelete.TabIndex = 28;
            this.BtnDelete.Text = "&Slet";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(6, 95);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(94, 23);
            this.BtnSave.TabIndex = 20;
            this.BtnSave.Text = "&Gem";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // DtpClosingDate
            // 
            this.DtpClosingDate.Location = new System.Drawing.Point(469, 69);
            this.DtpClosingDate.Name = "DtpClosingDate";
            this.DtpClosingDate.Size = new System.Drawing.Size(227, 20);
            this.DtpClosingDate.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(466, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Lukkedato";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(293, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Salgsdato";
            // 
            // DtpSaleDate
            // 
            this.DtpSaleDate.Location = new System.Drawing.Point(239, 69);
            this.DtpSaleDate.Name = "DtpSaleDate";
            this.DtpSaleDate.Size = new System.Drawing.Size(224, 20);
            this.DtpSaleDate.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Debitor";
            // 
            // CbDebtor
            // 
            this.CbDebtor.FormattingEnabled = true;
            this.CbDebtor.Location = new System.Drawing.Point(6, 68);
            this.CbDebtor.Name = "CbDebtor";
            this.CbDebtor.Size = new System.Drawing.Size(227, 21);
            this.CbDebtor.TabIndex = 14;
            this.CbDebtor.SelectedIndexChanged += new System.EventHandler(this.CbDebtor_SelectedIndexChanged);
            // 
            // TxtRemark
            // 
            this.TxtRemark.Location = new System.Drawing.Point(469, 27);
            this.TxtRemark.Name = "TxtRemark";
            this.TxtRemark.Size = new System.Drawing.Size(227, 20);
            this.TxtRemark.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(466, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Bemærkning";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(236, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Købsdato";
            // 
            // DtpBuyDate
            // 
            this.DtpBuyDate.Location = new System.Drawing.Point(239, 27);
            this.DtpBuyDate.Name = "DtpBuyDate";
            this.DtpBuyDate.Size = new System.Drawing.Size(224, 20);
            this.DtpBuyDate.TabIndex = 8;
            // 
            // CbxClosed
            // 
            this.CbxClosed.AutoSize = true;
            this.CbxClosed.Location = new System.Drawing.Point(704, 28);
            this.CbxClosed.Name = "CbxClosed";
            this.CbxClosed.Size = new System.Drawing.Size(42, 17);
            this.CbxClosed.TabIndex = 12;
            this.CbxClosed.Text = "Nej";
            this.CbxClosed.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(702, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Lukket";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Serienummer";
            // 
            // TxtSerialNumber
            // 
            this.TxtSerialNumber.Location = new System.Drawing.Point(6, 27);
            this.TxtSerialNumber.Name = "TxtSerialNumber";
            this.TxtSerialNumber.Size = new System.Drawing.Size(227, 20);
            this.TxtSerialNumber.TabIndex = 6;
            this.TxtSerialNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSerialNumber_KeyPress);
            // 
            // CreateVendorSerialNumbersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 506);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LblItemNo);
            this.Controls.Add(this.GboxTitle);
            this.Name = "CreateVendorSerialNumbersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Title";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateVendorSerialNumbersForm_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GboxTitle;
        private System.Windows.Forms.Label LblItemNo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtSerialNumber;
        private System.Windows.Forms.CheckBox CbxClosed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker DtpBuyDate;
        private System.Windows.Forms.TextBox TxtRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CbDebtor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker DtpSaleDate;
        private System.Windows.Forms.DateTimePicker DtpClosingDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
    }
}