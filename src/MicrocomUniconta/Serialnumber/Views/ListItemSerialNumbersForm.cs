﻿using MicrocomUniconta.Helper;
using MicrocomUniconta.Serialnumber.API;
using MicrocomUniconta.Serialnumber.Models;
using MicrocomUniconta.SerialNumber.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.Serialnumber.Views
{
    public partial class ListItemSerialNumbersForm : Form
    {        
        private readonly InvItemClient _invItem;        
        private ListViewEx<TblSerienummer> _lvSerialNumbers;
        private TblSerienummer _serienummer;
        private readonly SerialNumberUnicontaAPIHelpers api;


        public ListItemSerialNumbersForm(CrudAPI crudApi, TblSerienummer[] serialNumbers, InvItemClient invItem)
        {
            InitializeComponent();
            api = new SerialNumberUnicontaAPIHelpers(crudApi);
            _invItem = invItem;
            SetupDebtorCombobox();

            Text = $"{_invItem.Item} - {_invItem.Name}";
            LblInvItem.Text = $"{_invItem.Item} - {_invItem.Name}";                                    
            var sn = new List<TblSerienummer>();
            foreach (var item in serialNumbers)
            {
                sn.Add(item);
            }
            SetupListView(sn);            
        }

        private void SetupDebtorCombobox()
        {
            var task = Task.Run(async () => await api.GetAllDebtorsAsync());
            task.Wait();
            var debtorList = task.Result;
            CbDebtor.Items.Add("");
            foreach (var debtor in debtorList)
            {
                var debtorItem = new CustomComboboxItem()
                {
                    Text = $"{debtor.Account} - {debtor.Name}",
                    Value = debtor
                };
                CbDebtor.Items.Add(debtorItem);
            }            
        }

        private void SetupListView(List<TblSerienummer> serialNumbers)
        {
            GboxTitle.Controls.Remove(_lvSerialNumbers);
            var columnMapping = new List<(string ColumnName, Func<TblSerienummer, object> ValueLookup, Func<TblSerienummer, string> DisplayStringLookup)>()
            {
                ("Serienummer", s => s.Serienummer, s => s.Serienummer),
                ("Kreditor", s => s.Kreditor, s => s.Kreditor),
                ("Debitor", s => s.Debitor, s => s.Debitor),
                ("Varenummer", s => s.Varenummer, s => s.Varenummer),
                ("Varenavn", s => s.MasterName, s => s.MasterName),
                ("Købsdato", s => s.Kobdato, s => $"{s.Kobdato:dd MMM yyyy}"),
                ("Salgsdato", s => s.Salgsdato, s => $"{s.Salgsdato:dd MMM yyyy}"),
                ("Lukket", s => s.Lukket, s => s.Lukket ? "Ja" : "Nej"),
                ("Lukkedato", s => s.Lukkedato, s => $"{s.Lukkedato:dd MMM yyyy}"),
                ("Bemærkning", s => s.Bem, s => s.Bem)
            };

            _lvSerialNumbers = new ListViewEx<TblSerienummer>(columnMapping)
            {
                FullRowSelect = true,
                MultiSelect = false,
                View = View.Details,
                Left = 20,
                Top = 20,
                Width = 500,
                Height = 300,
                Dock = DockStyle.Fill
            };
            

            _lvSerialNumbers.AddRange(serialNumbers);
            _lvSerialNumbers.SelectedIndexChanged += LvSerialNumbers_SelectedIndexChanged;

            GboxTitle.Controls.Add(_lvSerialNumbers);
        }

        private void LvSerialNumbers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lvSerialNumbers.SelectedItems.Count > 0)
            {
                if (_lvSerialNumbers.FocusedItem.Index > -1)
                {
                    _serienummer = _lvSerialNumbers.FocusedItem.Tag as TblSerienummer;
                    SetFields();
                }
            }
        }

        private void SetFields()
        {
            for (int i = 1; i < CbDebtor.Items.Count; i++)
            {
                CbDebtor.SelectedIndex = i;
                var customCbItem = CbDebtor.SelectedItem as CustomComboboxItem;
                var debtorItem = customCbItem.Value as DebtorClient;
                if (debtorItem.Account == _serienummer.Debitor)
                {
                    break;
                }
                CbDebtor.SelectedIndex = 0;
            }

            if (_serienummer.Kobdato != DateTime.MinValue)
                DtpBuyDate.Value = _serienummer.Kobdato;
            if (_serienummer.Salgsdato != DateTime.MinValue)
                DtpSaleDate.Value = _serienummer.Salgsdato;
            CbxClosed.Checked = _serienummer.Lukket;
            if (_serienummer.Lukket)
            {
                CbxClosed.Text = "Ja";
            }
            else
            {
                CbxClosed.Text = "Nej";
            }
            if (_serienummer.Lukkedato != DateTime.MinValue)
                DtpClosingDate.Value = _serienummer.Lukkedato;
            TxtRemark.Text = _serienummer.Bem;
        }

        private void Save()
        {
            if (_serienummer != null)
            {
                if (CbDebtor.SelectedIndex > 0)
                {
                    var customCbItem = CbDebtor.SelectedItem as CustomComboboxItem;
                    var debtorItem = customCbItem.Value as DebtorClient;
                    _serienummer.Debitor = debtorItem.Account;
                }
                _serienummer.Kobdato = DtpBuyDate.Value;
                _serienummer.Salgsdato = DtpSaleDate.Value;
                _serienummer.Lukkedato = DtpSaleDate.Value;
                _serienummer.Lukket = CbxClosed.Checked;
                _serienummer.Bem = TxtRemark.Text;
                var task = Task.Run(async () => await api.UpdateSerialNumberAsync(_serienummer));
                task.Wait();
                var result = task.Result;
                if (result == ErrorCodes.Succes)
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Kunne ikke tilføje serienummeret.");
                }
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Save();

        }
    }
}
