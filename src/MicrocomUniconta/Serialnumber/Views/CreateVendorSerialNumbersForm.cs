﻿using MicrocomUniconta.Helper;
using MicrocomUniconta.Serialnumber.API;
using MicrocomUniconta.Serialnumber.Models;
using MicrocomUniconta.SerialNumber.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.Serialnumber.Views
{
    public partial class CreateVendorSerialNumbersForm : Form
    {
        private readonly SerialNumberUnicontaAPIHelpers api;        
        private readonly CreditorClient _vendor;
        private readonly InvItemClient _item;

        private ListViewEx<TblSerienummer> _lvSerialNumbers;
        private TblSerienummer _serienummer;
        private List<TblSerienummer> _newSerialNumbers;

        public CreateVendorSerialNumbersForm(CrudAPI crudApi, CreditorOrderLineClient vendorOrderLine)
        {
            InitializeComponent();
            api = new SerialNumberUnicontaAPIHelpers(crudApi);
            var task = Task.Run(async () => await api.GetVendorWhereAccountAsync(vendorOrderLine.Account));
            task.Wait();
            var vendor = task.Result;
            _vendor = vendor;
            _item = vendorOrderLine.InvItem;
            Text = $"{_vendor.Account} - {_vendor.Name}";
            LblItemNo.Text = $"{_item.Item} - {_item.Name}";            
            SetupDebtorCombobox();
            
            SetupListView(new List<TblSerienummer>());
        }        

        private void SetupListView(List<TblSerienummer> serialNumbers)
        {
            GboxTitle.Controls.Remove(_lvSerialNumbers);
            var columnMapping = new List<(string ColumnName, Func<TblSerienummer, object> ValueLookup, Func<TblSerienummer, string> DisplayStringLookup)>()
            {
                ("Serienummer", s => s.Serienummer, s => s.Serienummer),
                ("Kreditor", s => s.Kreditor, s => s.Kreditor),
                ("Debitor", s => s.Debitor, s => s.Debitor),
                ("Varenummer", s => s.Varenummer, s => s.Varenummer),
                ("Varenavn", s => s.MasterName, s => s.MasterName),
                ("Købsdato", s => s.Kobdato, s => $"{s.Kobdato:dd MMM yyyy}"),
                ("Salgsdato", s => s.Salgsdato, s => $"{s.Salgsdato:dd MMM yyyy}"),
                ("Lukket", s => s.Lukket, s => s.Lukket ? "Ja" : "Nej"),
                ("Lukkedato", s => s.Lukkedato, s => $"{s.Lukkedato:dd MMM yyyy}"),
                ("Bemærkning", s => s.Bem, s => s.Bem)
            };

            _lvSerialNumbers = new ListViewEx<TblSerienummer>(columnMapping)
            {
                FullRowSelect = true,
                MultiSelect = false,
                View = View.Details,
                Left = 20,
                Top = 20,
                Width = 500,
                Height = 300,
                Dock = DockStyle.Fill
            };

            _lvSerialNumbers.SelectedIndexChanged += SerienummerListView_SelectedIndexChanged;

            _lvSerialNumbers.AddRange(serialNumbers);

            GboxTitle.Controls.Add(_lvSerialNumbers);
        }

        private void SerienummerListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lvSerialNumbers.SelectedItems.Count > 0)
            {
                if (_lvSerialNumbers.FocusedItem.Index > -1)
                {
                    _serienummer = _lvSerialNumbers.FocusedItem.Tag as TblSerienummer;
                    SetFields();
                }
            }
        }

        private void SetFields()
        {
            TxtSerialNumber.Text = _serienummer.Serienummer;        

            if (_serienummer.Kobdato != DateTime.MinValue)
                DtpBuyDate.Value = _serienummer.Kobdato;
            if (_serienummer.Salgsdato != DateTime.MinValue)
                DtpSaleDate.Value = _serienummer.Salgsdato;
            CbxClosed.Checked = _serienummer.Lukket;
            if (_serienummer.Lukket)
            {
                CbxClosed.Text = "Ja";
            }
            else
            {
                CbxClosed.Text = "Nej";
            }
            if (_serienummer.Lukkedato != DateTime.MinValue)
                DtpClosingDate.Value = _serienummer.Lukkedato;
            TxtRemark.Text = _serienummer.Bem;
        }        
        
        private void SetupDebtorCombobox()
        {
            var task = Task.Run(async () => await api.GetAllDebtorsAsync());
            task.Wait();
            var debtorList = task.Result;
            CbDebtor.Items.Add("");
            foreach (var debtor in debtorList)
            {
                var debtorItem = new CustomComboboxItem()
                {
                    Text = $"{debtor.Account} - {debtor.Name}",
                    Value = debtor
                };
                CbDebtor.Items.Add(debtorItem);
            }
        }

        private TblSerienummer NewSerialNumber()
        {
            var output = new TblSerienummer();            

            if (CbDebtor.SelectedIndex > 0)
            {
                var customCbItem = CbDebtor.SelectedItem as CustomComboboxItem;
                var debtorItem = customCbItem.Value as DebtorClient;
                output.Debitor = debtorItem.Account;
            }
            
            output.Varenummer = _item.Item;
            output.SetMaster(_item);                                  
            output.Kreditor = _vendor.Account;
            output.Serienummer = TxtSerialNumber.Text;
            output.Kobdato = DateTime.Now;
            output.Salgsdato = DateTime.MinValue;
            output.Lukket = false;
            output.Lukkedato = DateTime.MinValue;
            output.Bem = TxtRemark.Text;
            return output;
        }        

        
        private void BtnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            var empty = _newSerialNumbers == null;            
            
            if (!empty)
            {
                var hasNewSerialnumbers = _newSerialNumbers.Count > 0;
                var existingSerialnumbersIsEmpty = _lvSerialNumbers.Items.Count == 0;
                if ((!empty) && (hasNewSerialnumbers || (existingSerialnumbersIsEmpty)))
                {
                    var task = Task.Run(async () => await api.InsertSerialNumbersAsync(_newSerialNumbers));
                    task.Wait();
                    var result = task.Result;
                    if (ErrorCodes.Succes == result)
                    {
                        _newSerialNumbers.Clear();
                    }
                    else
                    {
                        MessageBox.Show("Fejl ved oprettelse. Prøv igen");
                    }
                }
            }
            else
            {
                MessageBox.Show("Tilføj serienummer først");
            }
                       
        }

        private void TxtSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (string.IsNullOrEmpty(TxtSerialNumber.Text))
                {
                    //MessageBox.Show("Der skal være serienummer");
                    TxtSerialNumber.Focus();
                    TxtSerialNumber.BackColor = Color.Red;
                }
                else
                {
                    TxtSerialNumber.BackColor = Color.FromArgb(255,255,255,255);
                    if (_newSerialNumbers == null)
                    {
                        _newSerialNumbers = new List<TblSerienummer>();
                    }
                    var newSerialNumber = NewSerialNumber();
                    _newSerialNumbers.Add(newSerialNumber);
                    _lvSerialNumbers.Add(newSerialNumber);
                    TxtSerialNumber.Text = "";
                }                
            }
        }

        private void CbDebtor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CbDebtor.SelectedIndex > 0)
            {
                DtpSaleDate.Visible = true;
            }
            else
            {
                DtpSaleDate.Visible = false;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (_serienummer != null)
            {
                var dialog = MessageBox.Show($"Er du sikker du vil slette {_serienummer.Serienummer}?",
                    "Bekræft sletning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                if (dialog == DialogResult.Yes)
                {
                    // slet                    
                    var task = Task.Run(async () => await api.DeleteSerialNumberAsync(_serienummer));
                    task.Wait();
                    var result = task.Result;
                    if (ErrorCodes.Succes == result)
                    {
                        // Get the selected row
                        var selectedRow = _lvSerialNumbers.FocusedItem.Index;
                        try
                        {
                            _lvSerialNumbers.Remove(_serienummer); // Select in a list
                            if (selectedRow == _lvSerialNumbers.Items.Count)
                            {
                                if (_lvSerialNumbers.Items.Count != 0)
                                {
                                    _lvSerialNumbers.Items[selectedRow - 1].Selected = true;
                                }                                
                            }                            
                            else
                            {
                                _lvSerialNumbers.Items[selectedRow].Selected = true;
                            }
                        }
                        catch (Exception)
                        {
                            
                        }
                        
                        
                    }
                    else
                    {
                        MessageBox.Show("Fejl ved sletning");
                    }
                }
            }
        }

        private void TxtSerialNumber_TextChanged(object sender, EventArgs e)
        {            
        }

        private void CreateVendorSerialNumbersForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((_newSerialNumbers != null) && (_newSerialNumbers.Count > 0))
            {
                var dialog = MessageBox.Show($"Der er ugemte serienumre, er du sikker på du vil lukke vinduet?",
                    "Bekræft lukning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                if (dialog == DialogResult.Yes)
                {
                    
                }
                else
                {
                    e.Cancel = true;
                }
                
            }
        }
    }
}
