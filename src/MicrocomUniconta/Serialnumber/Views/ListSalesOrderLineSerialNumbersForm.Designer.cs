﻿
namespace MicrocomUniconta.Serialnumber.Views
{
    partial class ListSalesOrderLineSerialNumbersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GboxTitle = new System.Windows.Forms.GroupBox();
            this.LblInvItem = new System.Windows.Forms.Label();
            this.BtnSelectAndSave = new System.Windows.Forms.Button();
            this.TxtRemark = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // GboxTitle
            // 
            this.GboxTitle.Location = new System.Drawing.Point(9, 23);
            this.GboxTitle.Name = "GboxTitle";
            this.GboxTitle.Size = new System.Drawing.Size(822, 386);
            this.GboxTitle.TabIndex = 1;
            this.GboxTitle.TabStop = false;
            this.GboxTitle.Text = "Data";
            // 
            // LblInvItem
            // 
            this.LblInvItem.AutoSize = true;
            this.LblInvItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.LblInvItem.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LblInvItem.Location = new System.Drawing.Point(0, 0);
            this.LblInvItem.Name = "LblInvItem";
            this.LblInvItem.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.LblInvItem.Size = new System.Drawing.Size(29, 19);
            this.LblInvItem.TabIndex = 11;
            this.LblInvItem.Text = "Vare";
            // 
            // BtnSelectAndSave
            // 
            this.BtnSelectAndSave.Enabled = false;
            this.BtnSelectAndSave.Location = new System.Drawing.Point(721, 415);
            this.BtnSelectAndSave.Name = "BtnSelectAndSave";
            this.BtnSelectAndSave.Size = new System.Drawing.Size(106, 23);
            this.BtnSelectAndSave.TabIndex = 12;
            this.BtnSelectAndSave.Text = "Vælg og Gem";
            this.BtnSelectAndSave.UseVisualStyleBackColor = true;
            this.BtnSelectAndSave.Click += new System.EventHandler(this.BtnSelectAndSave_ClickAsync);
            // 
            // TxtRemark
            // 
            this.TxtRemark.Location = new System.Drawing.Point(488, 418);
            this.TxtRemark.Name = "TxtRemark";
            this.TxtRemark.Size = new System.Drawing.Size(227, 20);
            this.TxtRemark.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(415, 421);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Bemærkning";
            // 
            // ListSalesOrderLineSerialNumbersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 450);
            this.Controls.Add(this.TxtRemark);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.BtnSelectAndSave);
            this.Controls.Add(this.LblInvItem);
            this.Controls.Add(this.GboxTitle);
            this.Name = "ListSalesOrderLineSerialNumbersForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Title";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GboxTitle;
        private System.Windows.Forms.Label LblInvItem;
        private System.Windows.Forms.Button BtnSelectAndSave;
        private System.Windows.Forms.TextBox TxtRemark;
        private System.Windows.Forms.Label label8;
    }
}