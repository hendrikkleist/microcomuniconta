﻿using MicrocomUniconta.Helper;
using MicrocomUniconta.Serialnumber.API;
using MicrocomUniconta.Serialnumber.Models;
using MicrocomUniconta.SerialNumber.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.Serialnumber.Views
{
    public partial class ListSalesOrderLineSerialNumbersForm : Form
    {
        private readonly SerialNumberUnicontaAPIHelpers _api;
        private readonly DebtorOrderLineClient _debtorOrderLine;        
        private ListViewEx<TblSerienummer> _lvSerialNumbers;
        private TblSerienummer _serienummer;

        public ListSalesOrderLineSerialNumbersForm(CrudAPI crudAPI, TblSerienummer[] serialNumbers, DebtorOrderLineClient debtorOrderLine)
        {
            InitializeComponent();
            _api = new SerialNumberUnicontaAPIHelpers(crudAPI);
            _debtorOrderLine = debtorOrderLine;            
            var labelText = $"{_debtorOrderLine.InvItem.Item} - {_debtorOrderLine.InvItem.Name}, {_debtorOrderLine.Account} - {_debtorOrderLine.Name}, {_debtorOrderLine.PurchaseAccountRef.Account} - {_debtorOrderLine.PurchaseAccountRef.Name}";
            Text = labelText;
            LblInvItem.Text = labelText;
            var sn = new List<TblSerienummer>();
            foreach (var item in serialNumbers)
            {
                if (string.IsNullOrEmpty(item.Debitor))
                    sn.Add(item);
            }
            SetupListView(sn);            
        }        

        private void SetupListView(List<TblSerienummer> serialNumbers)
        {
            GboxTitle.Controls.Remove(_lvSerialNumbers);
            var columnMapping = new List<(string ColumnName, Func<TblSerienummer, object> ValueLookup, Func<TblSerienummer, string> DisplayStringLookup)>()
            {
                ("Debitor", s => s.Debitor, s => s.Debitor),
                ("Serienummer", s => s.Serienummer, s => s.Serienummer),
                ("Kreditor", s => s.Kreditor, s => s.Kreditor),
                ("Varenummer", s => s.Varenummer, s => s.Varenummer),
                ("Varenavn", s => s.MasterName, s => s.MasterName),
                ("Købsdato", s => s.Kobdato, s => $"{s.Kobdato:dd MMM yyyy}"),
                ("Salgsdato", s => s.Salgsdato, s => $"{s.Salgsdato:dd MMM yyyy}"),
                ("Lukket", s => s.Lukket, s => s.Lukket ? "Ja" : "Nej"),
                ("Lukkedato", s => s.Lukkedato, s => $"{s.Lukkedato:dd MMM yyyy}"),
                ("Bemærkning", s => s.Bem, s => s.Bem)
            };

            _lvSerialNumbers = new ListViewEx<TblSerienummer>(columnMapping)
            {
                FullRowSelect = true,
                MultiSelect = false,
                View = View.Details,
                Left = 20,
                Top = 20,
                Width = 500,
                Height = 300,
                Dock = DockStyle.Fill
            };

            _lvSerialNumbers.SelectedIndexChanged += LvSerialNumbers_SelectedIndexChanged;
            

            _lvSerialNumbers.AddRange(serialNumbers);            
            GboxTitle.Controls.Add(_lvSerialNumbers);
        }

        private void LvSerialNumbers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lvSerialNumbers.SelectedItems.Count > 0)
            {
                if (_lvSerialNumbers.FocusedItem.Index > -1)
                {
                    _serienummer = _lvSerialNumbers.FocusedItem.Tag as TblSerienummer;
                    BtnSelectAndSave.Enabled = true;
                }
            }
            else
            {
                BtnSelectAndSave.Enabled = false;
            }
        }

        private async void BtnSelectAndSave_ClickAsync(object sender, EventArgs e)
        {
            _serienummer.Debitor = _debtorOrderLine.Account;
            _serienummer.Bem = TxtRemark.Text;
            _serienummer.Salgsdato = DateTime.Now;
            var call = await _api.UpdateSerialNumberAsync(_serienummer);
            if (call == ErrorCodes.Succes)
                DialogResult = DialogResult.OK;
            else
                MessageBox.Show("Fejl ved opdatering");
        }
    }
}
