﻿using MicrocomUniconta.Serialnumber.API;
using MicrocomUniconta.SerialNumber.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.Common;

namespace MicrocomUniconta.Serialnumber
{
    public class SerialNumberMasterKeyFix : IPluginBase
    {        
        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "fiks_serienumre";

        public event EventHandler OnExecute;

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {            

            var serialNumberAPI = new SerialNumberUnicontaAPIHelpers(_crudApi);
            var itemAPI = new InvItemUnicontaAPIHelpers(_crudApi);
            var serialNumbersTask = Task.Run(async () => await serialNumberAPI.GetAllSerialNumbers());
            serialNumbersTask.Wait();
            var serialNumbers = serialNumbersTask.Result;
            var newSerialNumbers = new List<TblSerienummer>();
            var deleteSerialNumbers = new List<TblSerienummer>();
            foreach (var serialNumber in serialNumbers)
            {
                if (serialNumber.MasterKey == null || serialNumber.MasterKey == "")
                {
                    deleteSerialNumbers.Add(serialNumber);                    
                }
            }
            var deleteSerialNumbersTask = Task.Run(async () => await serialNumberAPI.DeleteSerialNumbersAsync(deleteSerialNumbers));
            deleteSerialNumbersTask.Wait();

            foreach (var serialNumber in serialNumbers)
            {
                if (serialNumber.MasterKey == null || serialNumber.MasterKey == "")
                {                    
                    var itemNo = serialNumber.Varenummer;
                    var itemTask = Task.Run(async () => await itemAPI.GetItem(itemNo));
                    itemTask.Wait();
                    var invItem = itemTask.Result;
                    serialNumber.SetMaster(invItem);
                    newSerialNumbers.Add(serialNumber);
                }
            }

            var insertNewSerialNumbersTask = Task.Run(async () => await serialNumberAPI.InsertSerialNumbersAsync(newSerialNumbers));
            insertNewSerialNumbersTask.Wait();
            return ErrorCodes.Succes;            
        }

        #region Uniconta Setup

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {

        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters)
        {
        }

        #endregion

    }
}
