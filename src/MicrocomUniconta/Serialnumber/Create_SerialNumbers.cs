﻿using MicrocomUniconta.Serialnumber.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.Serialnumber
{
    public class Create_SerialNumbers : IPluginBase
    {
        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "opret_serienumre_for_kreditor";
        
        public event EventHandler OnExecute;

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {
            CreditorOrderLineClient creditorOrderLineClient = (CreditorOrderLineClient)currentRow;
            if (creditorOrderLineClient.Account != null)
            {
                var createVendorSerialNumbersForm = new CreateVendorSerialNumbersForm(_crudApi, creditorOrderLineClient);                
                if (createVendorSerialNumbersForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    createVendorSerialNumbersForm.Dispose();
                    return ErrorCodes.Succes;
                }
                else
                {
                    createVendorSerialNumbersForm.Dispose();
                    return ErrorCodes.Succes;
                }
            }
            return ErrorCodes.Succes;
        }


        #region Uniconta Setup

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {

        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters)
        {
        }

        #endregion
    }
}
