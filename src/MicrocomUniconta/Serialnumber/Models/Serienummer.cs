﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.Common;
using Uniconta.DataModel;

namespace MicrocomUniconta.SerialNumber.Models
{
	public class TblSerienummer : TableData
	{
        public override int UserDefinedId { get { return 1323; } }		
		
		public override Type MasterType { get { return typeof(Uniconta.DataModel.InvItem); } }

		[ForeignKeyAttribute(ForeignKeyTable = typeof(InvItem))]
		[Display(Name = "Varenummer")]
		public string Varenummer
		{
			get { return this.GetUserFieldString(0); }
			set { this.SetUserFieldString(0, value); NotifyPropertyChanged("Varenummer"); }
		}

		[ForeignKeyAttribute(ForeignKeyTable = typeof(Creditor))]
		[Display(Name = "Kreditor")]
		public string Kreditor
		{
			get { return this.GetUserFieldString(1); }
			set { this.SetUserFieldString(1, value); NotifyPropertyChanged("Kreditor"); }
		}

		[ForeignKeyAttribute(ForeignKeyTable = typeof(Debtor))]
		[Display(Name = "Debitor")]
		public string Debitor
		{
			get { return this.GetUserFieldString(2); }
			set { this.SetUserFieldString(2, value); NotifyPropertyChanged("Debitor"); }
		}

		[Display(Name = "Serienummer")]
		public string Serienummer
		{
			get { return this.GetUserFieldString(3); }
			set { this.SetUserFieldString(3, value); NotifyPropertyChanged("Serienummer"); }
		}

		[Display(Name = "Købsdato")]
		public DateTime Kobdato
		{
			get { return this.GetUserFieldDateTime(4); }
			set { this.SetUserFieldDateTime(4, value); NotifyPropertyChanged("Kobdato"); }
		}

		[Display(Name = "Salgsdato")]
		public DateTime Salgsdato
		{
			get { return this.GetUserFieldDateTime(5); }
			set { this.SetUserFieldDateTime(5, value); NotifyPropertyChanged("Salgsdato"); }
		}

		[Display(Name = "Lukket")]
		public bool Lukket
		{
			get { return this.GetUserFieldBoolean(6); }
			set { this.SetUserFieldBoolean(6, value); NotifyPropertyChanged("Lukket"); }
		}

		[Display(Name = "Lukkedato")]
		public DateTime Lukkedato
		{
			get { return this.GetUserFieldDateTime(7); }
			set { this.SetUserFieldDateTime(7, value); NotifyPropertyChanged("Lukkedato"); }
		}

		[Display(Name = "Bemærkning")]
		public string Bem
		{
			get { return this.GetUserFieldString(8); }
			set { this.SetUserFieldString(8, value); NotifyPropertyChanged("Bem"); }
		}
    }

}
