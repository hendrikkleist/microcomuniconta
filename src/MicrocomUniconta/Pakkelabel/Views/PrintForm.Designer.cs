﻿namespace MicrocomUniconta.Pakkelabel.Views
{
    partial class PrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtName = new System.Windows.Forms.TextBox();
            this.TxtAddress1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtAddress2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtZipCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtCity = new System.Windows.Forms.TextBox();
            this.TxtDate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtWeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ParcelValue = new System.Windows.Forms.GroupBox();
            this.Rbt20000 = new System.Windows.Forms.RadioButton();
            this.Rbt1000 = new System.Windows.Forms.RadioButton();
            this.Rbt0 = new System.Windows.Forms.RadioButton();
            this.TxtAtt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtCountry = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.GbShippingAgent = new System.Windows.Forms.GroupBox();
            this.RbtShipmondo = new System.Windows.Forms.RadioButton();
            this.RbtGLSInline = new System.Windows.Forms.RadioButton();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.RbtDelivery = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtDeliveryCountry = new System.Windows.Forms.TextBox();
            this.TxtDeliveryName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtDeliveryAtt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtDeliveryAddress1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtDeliveryAddress2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtDeliveryZipCode = new System.Windows.Forms.TextBox();
            this.TxtDeliveryCity = new System.Windows.Forms.TextBox();
            this.RbtDebitorVendor = new System.Windows.Forms.RadioButton();
            this.ParcelValue.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GbShippingAgent.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Navn";
            // 
            // TxtName
            // 
            this.TxtName.Location = new System.Drawing.Point(87, 19);
            this.TxtName.Name = "TxtName";
            this.TxtName.Size = new System.Drawing.Size(425, 20);
            this.TxtName.TabIndex = 1;
            this.TxtName.TextChanged += new System.EventHandler(this.TxtName_TextChanged);
            // 
            // TxtAddress1
            // 
            this.TxtAddress1.Location = new System.Drawing.Point(87, 45);
            this.TxtAddress1.Name = "TxtAddress1";
            this.TxtAddress1.Size = new System.Drawing.Size(425, 20);
            this.TxtAddress1.TabIndex = 2;
            this.TxtAddress1.TextChanged += new System.EventHandler(this.TxtAddress1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Addresse 1";
            // 
            // TxtAddress2
            // 
            this.TxtAddress2.Location = new System.Drawing.Point(87, 71);
            this.TxtAddress2.Name = "TxtAddress2";
            this.TxtAddress2.Size = new System.Drawing.Size(425, 20);
            this.TxtAddress2.TabIndex = 3;
            this.TxtAddress2.TextChanged += new System.EventHandler(this.TxtAddress2_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Addresse 2";
            // 
            // TxtZipCode
            // 
            this.TxtZipCode.Location = new System.Drawing.Point(87, 97);
            this.TxtZipCode.Name = "TxtZipCode";
            this.TxtZipCode.Size = new System.Drawing.Size(121, 20);
            this.TxtZipCode.TabIndex = 4;
            this.TxtZipCode.TextChanged += new System.EventHandler(this.TxtZipCode_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Postnr/By";
            // 
            // TxtCity
            // 
            this.TxtCity.Location = new System.Drawing.Point(214, 97);
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Size = new System.Drawing.Size(298, 20);
            this.TxtCity.TabIndex = 5;
            this.TxtCity.TextChanged += new System.EventHandler(this.TxtCity_TextChanged);
            // 
            // TxtDate
            // 
            this.TxtDate.Location = new System.Drawing.Point(87, 19);
            this.TxtDate.Name = "TxtDate";
            this.TxtDate.Size = new System.Drawing.Size(121, 20);
            this.TxtDate.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Dato";
            // 
            // TxtWeight
            // 
            this.TxtWeight.Location = new System.Drawing.Point(87, 45);
            this.TxtWeight.Name = "TxtWeight";
            this.TxtWeight.Size = new System.Drawing.Size(121, 20);
            this.TxtWeight.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Vægt";
            // 
            // ParcelValue
            // 
            this.ParcelValue.Controls.Add(this.Rbt20000);
            this.ParcelValue.Controls.Add(this.Rbt1000);
            this.ParcelValue.Controls.Add(this.Rbt0);
            this.ParcelValue.Location = new System.Drawing.Point(87, 71);
            this.ParcelValue.Name = "ParcelValue";
            this.ParcelValue.Size = new System.Drawing.Size(121, 101);
            this.ParcelValue.TabIndex = 14;
            this.ParcelValue.TabStop = false;
            this.ParcelValue.Text = "Pakkeværdi";
            // 
            // Rbt20000
            // 
            this.Rbt20000.AutoSize = true;
            this.Rbt20000.Location = new System.Drawing.Point(6, 71);
            this.Rbt20000.Name = "Rbt20000";
            this.Rbt20000.Size = new System.Drawing.Size(55, 17);
            this.Rbt20000.TabIndex = 2;
            this.Rbt20000.TabStop = true;
            this.Rbt20000.Text = "20000";
            this.Rbt20000.UseVisualStyleBackColor = true;
            // 
            // Rbt1000
            // 
            this.Rbt1000.AutoSize = true;
            this.Rbt1000.Location = new System.Drawing.Point(6, 45);
            this.Rbt1000.Name = "Rbt1000";
            this.Rbt1000.Size = new System.Drawing.Size(55, 17);
            this.Rbt1000.TabIndex = 1;
            this.Rbt1000.TabStop = true;
            this.Rbt1000.Text = "10000";
            this.Rbt1000.UseVisualStyleBackColor = true;
            // 
            // Rbt0
            // 
            this.Rbt0.AutoSize = true;
            this.Rbt0.Location = new System.Drawing.Point(6, 19);
            this.Rbt0.Name = "Rbt0";
            this.Rbt0.Size = new System.Drawing.Size(31, 17);
            this.Rbt0.TabIndex = 0;
            this.Rbt0.TabStop = true;
            this.Rbt0.Text = "0";
            this.Rbt0.UseVisualStyleBackColor = true;
            // 
            // TxtAtt
            // 
            this.TxtAtt.Location = new System.Drawing.Point(87, 149);
            this.TxtAtt.Name = "TxtAtt";
            this.TxtAtt.Size = new System.Drawing.Size(425, 20);
            this.TxtAtt.TabIndex = 7;
            this.TxtAtt.TextChanged += new System.EventHandler(this.TxtAtt_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(61, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Att";
            // 
            // BtnPrint
            // 
            this.BtnPrint.Location = new System.Drawing.Point(133, 178);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(75, 23);
            this.BtnPrint.TabIndex = 17;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.UseVisualStyleBackColor = true;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_ClickAsync);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.TxtCountry);
            this.groupBox1.Controls.Add(this.TxtName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TxtAtt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TxtAddress1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtAddress2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtZipCode);
            this.groupBox1.Controls.Add(this.TxtCity);
            this.groupBox1.Location = new System.Drawing.Point(6, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(540, 179);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Debitor/Kreditor";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(50, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "Land";
            // 
            // TxtCountry
            // 
            this.TxtCountry.Location = new System.Drawing.Point(87, 123);
            this.TxtCountry.Name = "TxtCountry";
            this.TxtCountry.Size = new System.Drawing.Size(425, 20);
            this.TxtCountry.TabIndex = 6;
            this.TxtCountry.TextChanged += new System.EventHandler(this.TxtCountry_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.GbShippingAgent);
            this.groupBox2.Controls.Add(this.BtnCancel);
            this.groupBox2.Controls.Add(this.TxtDate);
            this.groupBox2.Controls.Add(this.ParcelValue);
            this.groupBox2.Controls.Add(this.TxtWeight);
            this.groupBox2.Controls.Add(this.BtnPrint);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(12, 455);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(558, 211);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pakke";
            // 
            // GbShippingAgent
            // 
            this.GbShippingAgent.Controls.Add(this.RbtShipmondo);
            this.GbShippingAgent.Controls.Add(this.RbtGLSInline);
            this.GbShippingAgent.Location = new System.Drawing.Point(220, 19);
            this.GbShippingAgent.Name = "GbShippingAgent";
            this.GbShippingAgent.Size = new System.Drawing.Size(245, 153);
            this.GbShippingAgent.TabIndex = 19;
            this.GbShippingAgent.TabStop = false;
            this.GbShippingAgent.Text = "Vælg shipping agent";
            // 
            // RbtShipmondo
            // 
            this.RbtShipmondo.AutoSize = true;
            this.RbtShipmondo.Location = new System.Drawing.Point(6, 42);
            this.RbtShipmondo.Name = "RbtShipmondo";
            this.RbtShipmondo.Size = new System.Drawing.Size(78, 17);
            this.RbtShipmondo.TabIndex = 2;
            this.RbtShipmondo.TabStop = true;
            this.RbtShipmondo.Text = "Shipmondo";
            this.RbtShipmondo.UseVisualStyleBackColor = true;
            // 
            // RbtGLSInline
            // 
            this.RbtGLSInline.AutoSize = true;
            this.RbtGLSInline.Location = new System.Drawing.Point(6, 19);
            this.RbtGLSInline.Name = "RbtGLSInline";
            this.RbtGLSInline.Size = new System.Drawing.Size(73, 17);
            this.RbtGLSInline.TabIndex = 1;
            this.RbtGLSInline.TabStop = true;
            this.RbtGLSInline.Text = "GLS inline";
            this.RbtGLSInline.UseVisualStyleBackColor = true;
            // 
            // BtnCancel
            // 
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.BtnCancel.Location = new System.Drawing.Point(52, 178);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 18;
            this.BtnCancel.Text = "Annuller";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.RbtDelivery);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Controls.Add(this.RbtDebitorVendor);
            this.groupBox4.Controls.Add(this.groupBox1);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(558, 437);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Leveringsmetode";
            // 
            // RbtDelivery
            // 
            this.RbtDelivery.AutoSize = true;
            this.RbtDelivery.Location = new System.Drawing.Point(6, 226);
            this.RbtDelivery.Name = "RbtDelivery";
            this.RbtDelivery.Size = new System.Drawing.Size(108, 17);
            this.RbtDelivery.TabIndex = 1;
            this.RbtDelivery.TabStop = true;
            this.RbtDelivery.Text = "Leveringsadresse";
            this.RbtDelivery.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.TxtDeliveryCountry);
            this.groupBox3.Controls.Add(this.TxtDeliveryName);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.TxtDeliveryAtt);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.TxtDeliveryAddress1);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.TxtDeliveryAddress2);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.TxtDeliveryZipCode);
            this.groupBox3.Controls.Add(this.TxtDeliveryCity);
            this.groupBox3.Location = new System.Drawing.Point(6, 249);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(540, 179);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Leveringsadresse";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(50, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Land";
            // 
            // TxtDeliveryCountry
            // 
            this.TxtDeliveryCountry.Location = new System.Drawing.Point(87, 123);
            this.TxtDeliveryCountry.Name = "TxtDeliveryCountry";
            this.TxtDeliveryCountry.Size = new System.Drawing.Size(425, 20);
            this.TxtDeliveryCountry.TabIndex = 6;
            // 
            // TxtDeliveryName
            // 
            this.TxtDeliveryName.Location = new System.Drawing.Point(87, 19);
            this.TxtDeliveryName.Name = "TxtDeliveryName";
            this.TxtDeliveryName.Size = new System.Drawing.Size(425, 20);
            this.TxtDeliveryName.TabIndex = 1;
            this.TxtDeliveryName.TextChanged += new System.EventHandler(this.TxtDeliveryName_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Navn";
            // 
            // TxtDeliveryAtt
            // 
            this.TxtDeliveryAtt.Location = new System.Drawing.Point(87, 149);
            this.TxtDeliveryAtt.Name = "TxtDeliveryAtt";
            this.TxtDeliveryAtt.Size = new System.Drawing.Size(425, 20);
            this.TxtDeliveryAtt.TabIndex = 7;
            this.TxtDeliveryAtt.TextChanged += new System.EventHandler(this.TxtDeliveryAtt_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Addresse 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(61, 152);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Att";
            // 
            // TxtDeliveryAddress1
            // 
            this.TxtDeliveryAddress1.Location = new System.Drawing.Point(87, 45);
            this.TxtDeliveryAddress1.Name = "TxtDeliveryAddress1";
            this.TxtDeliveryAddress1.Size = new System.Drawing.Size(425, 20);
            this.TxtDeliveryAddress1.TabIndex = 2;
            this.TxtDeliveryAddress1.TextChanged += new System.EventHandler(this.TxtDeliveryAddress1_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Addresse 2";
            // 
            // TxtDeliveryAddress2
            // 
            this.TxtDeliveryAddress2.Location = new System.Drawing.Point(87, 71);
            this.TxtDeliveryAddress2.Name = "TxtDeliveryAddress2";
            this.TxtDeliveryAddress2.Size = new System.Drawing.Size(425, 20);
            this.TxtDeliveryAddress2.TabIndex = 3;
            this.TxtDeliveryAddress2.TextChanged += new System.EventHandler(this.TxtDeliveryAddress2_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(27, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Postnr/By";
            // 
            // TxtDeliveryZipCode
            // 
            this.TxtDeliveryZipCode.Location = new System.Drawing.Point(87, 97);
            this.TxtDeliveryZipCode.Name = "TxtDeliveryZipCode";
            this.TxtDeliveryZipCode.Size = new System.Drawing.Size(121, 20);
            this.TxtDeliveryZipCode.TabIndex = 4;
            this.TxtDeliveryZipCode.TextChanged += new System.EventHandler(this.TxtDeliveryZipCode_TextChanged);
            // 
            // TxtDeliveryCity
            // 
            this.TxtDeliveryCity.Location = new System.Drawing.Point(214, 97);
            this.TxtDeliveryCity.Name = "TxtDeliveryCity";
            this.TxtDeliveryCity.Size = new System.Drawing.Size(298, 20);
            this.TxtDeliveryCity.TabIndex = 5;
            this.TxtDeliveryCity.TextChanged += new System.EventHandler(this.TxtDeliveryCity_TextChanged);
            // 
            // RbtDebitorVendor
            // 
            this.RbtDebitorVendor.AutoSize = true;
            this.RbtDebitorVendor.Location = new System.Drawing.Point(6, 19);
            this.RbtDebitorVendor.Name = "RbtDebitorVendor";
            this.RbtDebitorVendor.Size = new System.Drawing.Size(100, 17);
            this.RbtDebitorVendor.TabIndex = 0;
            this.RbtDebitorVendor.TabStop = true;
            this.RbtDebitorVendor.Text = "Debitor/Kreditor";
            this.RbtDebitorVendor.UseVisualStyleBackColor = true;
            // 
            // PrintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 677);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Name = "PrintForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Print Pakkelabel";
            this.ParcelValue.ResumeLayout(false);
            this.ParcelValue.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.GbShippingAgent.ResumeLayout(false);
            this.GbShippingAgent.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtName;
        private System.Windows.Forms.TextBox TxtAddress1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtAddress2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtZipCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtCity;
        private System.Windows.Forms.TextBox TxtDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtWeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox ParcelValue;
        private System.Windows.Forms.RadioButton Rbt20000;
        private System.Windows.Forms.RadioButton Rbt1000;
        private System.Windows.Forms.RadioButton Rbt0;
        private System.Windows.Forms.TextBox TxtAtt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton RbtDelivery;
        private System.Windows.Forms.RadioButton RbtDebitorVendor;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TxtDeliveryName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtDeliveryAtt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtDeliveryAddress1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TxtDeliveryAddress2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxtDeliveryZipCode;
        private System.Windows.Forms.TextBox TxtDeliveryCity;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtCountry;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox TxtDeliveryCountry;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.GroupBox GbShippingAgent;
        private System.Windows.Forms.RadioButton RbtShipmondo;
        private System.Windows.Forms.RadioButton RbtGLSInline;
    }
}