﻿using MicrocomUniconta.Pakkelabel.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;
using Uniconta.DataModel;

namespace MicrocomUniconta.Pakkelabel.Views
{
    public partial class PrintForm : Form
    {
        private readonly CrudAPI _crudApi;

        private readonly DebtorClient _debtor;
        private readonly CreditorClient _creditor;
        private readonly DebtorOrderClient _order;
        private readonly PrintType _printType;
        private readonly string _receiver_country = "DK";

        public PrintForm(CrudAPI crudApi, object toPrint, PrintType printType)
        {
            InitializeComponent();

            _crudApi = crudApi;
            _printType = printType;
            Rbt0.Checked = true;
            RbtGLSInline.Checked = true;

            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor = (DebtorClient)toPrint;
                    DebitorToGUI(_debtor);
                    break;
                case PrintType.Vendor:
                    _creditor = (CreditorClient)toPrint;
                    VendorToGUI(_creditor);
                    break;
                default:
                    break;
            }
            TxtDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
        }  
        
        public PrintForm(CrudAPI crudAPI, DebtorOrderClient debtorOrder, DebtorClient debtorClient, PrintType printType)
        {
            InitializeComponent();

            _crudApi = crudAPI;
            _printType = printType;
            Rbt0.Checked = true;
            RbtGLSInline.Checked = true;
            _debtor = debtorClient;
            DebitorToGUI(_debtor);

            if(printType == PrintType.Order)
            {
                TxtDeliveryName.Text = debtorOrder.DeliveryName;
                TxtDeliveryAddress1.Text = debtorOrder.DeliveryAddress1;
                TxtDeliveryAddress2.Text = debtorOrder.DeliveryAddress2;
                TxtDeliveryZipCode.Text = debtorOrder.DeliveryZipCode;
                TxtDeliveryCity.Text = debtorOrder.DeliveryCity;
                TxtDeliveryCountry.Text = _receiver_country;
                TxtDeliveryAtt.Text = debtorOrder.DeliveryName;
                TxtWeight.Text = "1";                
                if (!string.IsNullOrEmpty(debtorOrder.DeliveryAddress1))
                {
                    RbtDelivery.Checked = true;
                }
            }
                
            _order = debtorOrder;
            var weight = 0.0;
            foreach (var item in _order.Lines)
            {
                weight += item.Weight;
            }
            if (weight == 0)
            { 
                weight = 1;
            }
            TxtWeight.Text = weight.ToString();


            Text = $"Print pakkelabel - Ordrenummer: {_order.OrderNumber}";
            TxtDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
        }

        private void DebitorToGUI(DebtorClient debtor)
        {
            if (!string.IsNullOrEmpty(debtor.DeliveryAddress1))
                RbtDelivery.Checked = true;
            else
                RbtDebitorVendor.Checked = true;

            TxtName.Text = debtor.Name;
            TxtAddress1.Text = debtor.Address1;
            TxtAddress2.Text = debtor.Address2;
            TxtZipCode.Text = debtor.ZipCode;
            TxtDeliveryCity.Text = debtor.DeliveryCity;
            TxtCity.Text = debtor.City;
            TxtCountry.Text = _receiver_country;
            TxtAtt.Text = debtor.Name;
            if (string.IsNullOrEmpty(debtor.ContactPerson))
            {
                debtor.ContactPerson = debtor.Name;
            }               

            TxtDeliveryName.Text = debtor.DeliveryName;
            TxtDeliveryAddress1.Text = debtor.DeliveryAddress1;
            TxtDeliveryAddress2.Text = debtor.DeliveryAddress2;
            TxtDeliveryZipCode.Text = debtor.DeliveryZipCode;
            TxtDeliveryCity.Text = debtor.DeliveryCity;
            TxtDeliveryCountry.Text = _receiver_country;
            TxtDeliveryAtt.Text = debtor.DeliveryName;
            TxtWeight.Text = "1";
        }

        private void VendorToGUI(CreditorClient creditor)
        {
            if (!string.IsNullOrEmpty(creditor.DeliveryAddress1))
                RbtDelivery.Checked = true;
            else
                RbtDebitorVendor.Checked = true;

            TxtName.Text = creditor.Name;
            TxtAddress1.Text = creditor.Address1;
            TxtAddress2.Text = creditor.Address2;
            TxtZipCode.Text = creditor.ZipCode;
            TxtCity.Text = creditor.City;
            TxtCountry.Text = _receiver_country;
            TxtAtt.Text = creditor.ContactPerson;

            if (string.IsNullOrEmpty(creditor.ContactPerson))
            {
                creditor.ContactPerson = creditor.Name;
            }


            TxtDeliveryName.Text = creditor.DeliveryName;
            TxtDeliveryAddress1.Text = creditor.DeliveryAddress1;
            TxtDeliveryAddress2.Text = creditor.DeliveryAddress2;
            TxtDeliveryZipCode.Text = creditor.DeliveryZipCode;
            TxtDeliveryCity.Text = creditor.DeliveryCity;
            TxtDeliveryCountry.Text = _receiver_country;
            TxtDeliveryAtt.Text = creditor.ContactPerson;
            TxtWeight.Text = "1";
        }


        private async void BtnPrint_ClickAsync(object sender, EventArgs e)
        {
            Double.TryParse(TxtWeight.Text, out double weight);
            if (weight.Equals(0))
            {
                MessageBox.Show("Vægt skal være større end 0");
            }
            else if (weight > 20)
            {
                MessageBox.Show("Vægt skal være max 20 kg");
            }
            else
            {
                var criteria = new List<PropValuePair>();
                var shippingAgent = RbtGLSInline.Checked ? "gls" : "shipmondo";

                var whereClause = PropValuePair.GenereteWhereElements("ShippingAgent", typeof(string), shippingAgent);
                criteria.Add(whereClause);
                Pakkelabels[] pakkelabels = await _crudApi.Query(new Pakkelabels(), null, criteria);
                if (pakkelabels.Count() > 0)
                {
                    var pakkelabel = pakkelabels[0];
                   
                    var services = "";
                    if (RbtGLSInline.Checked)
                    {
                        if (Rbt1000.Checked)
                        {
                            services = "10000";
                        }
                        else if (Rbt20000.Checked)
                        {
                            services = "20000";
                        }
                    }
                    if (RbtShipmondo.Checked)
                    {
                        if (Rbt1000.Checked)
                        {
                            services = "27";
                        }
                        else if (Rbt20000.Checked)
                        {
                            services = "28";
                        }
                    }
                    DateTime date = DateTime.ParseExact(TxtDate.Text, "dd-MM-yyyy", CultureInfo.CurrentCulture);
                    var isDelivery = RbtDelivery.Checked;
                    string convertedText = "";
                    if (RbtGLSInline.Checked)
                    {
                        switch (_printType)
                        {
                            case PrintType.Debitor:
                                var debtorConverter = new Converter(isDelivery, _printType, pakkelabel, _debtor, services, weight.ToString(), date, _receiver_country);
                                convertedText = debtorConverter.GLSOutput();
                                break;
                            case PrintType.Vendor:
                                var vendorConverter = new Converter(isDelivery, _printType, pakkelabel, _creditor, services, weight.ToString(), date, _receiver_country);
                                convertedText = vendorConverter.GLSOutput();
                                break;
                            case PrintType.Order:
                                var orderConverter = new Converter(isDelivery, _printType, pakkelabel, _order, _debtor, services, weight.ToString(), date, _receiver_country);
                                convertedText = orderConverter.GLSOutput();
                                break;
                            default:
                                break;
                        }
                    }
                    if (RbtShipmondo.Checked)
                    {
                        weight *= 1000;
                        switch (_printType)
                        {
                            case PrintType.Debitor:
                                var debtorConverter = new Converter(isDelivery, _printType, pakkelabel, _debtor, services, weight.ToString(), date, _receiver_country);
                                convertedText = debtorConverter.ShipmondoOutput();                                
                                break;
                            case PrintType.Vendor:
                                var vendorConverter = new Converter(isDelivery, _printType, pakkelabel, _creditor, services, weight.ToString(), date, _receiver_country);
                                convertedText = vendorConverter.ShipmondoOutput();                                
                                break;
                            case PrintType.Order:
                                var orderConverter = new Converter(isDelivery, _printType, pakkelabel, _order, _debtor, services, weight.ToString(), date, _receiver_country);
                                convertedText = orderConverter.ShipmondoOutput();                                
                                break;
                            default:
                                break;
                        }
                    }
                    File.WriteAllText(pakkelabel.FileOutputPath, convertedText, Encoding.GetEncoding(1252));
                    DialogResult = DialogResult.OK;
                }
            }
        }

        #region Events

        private void TxtName_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.Name = TxtName.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.Name = TxtName.Text;
                    break;
                case PrintType.Order:
                    _debtor.Name = TxtName.Text;
                    // TODO: Add the name to the order
                    break;
                default:
                    break;
            }
        }

        private void TxtAddress1_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.Address1 = TxtAddress1.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.Address1 = TxtAddress1.Text;
                    break;
                case PrintType.Order:                   
                    _debtor.Address1 = TxtAddress1.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtAddress2_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.Address2 = TxtAddress2.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.Address2 = TxtAddress2.Text;
                    break;
                case PrintType.Order:
                    _debtor.Address2 = TxtAddress2.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtZipCode_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.ZipCode = TxtZipCode.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.ZipCode = TxtZipCode.Text;
                    break;
                case PrintType.Order:
                    _debtor.ZipCode = TxtZipCode.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtCity_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.City = TxtCity.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.City = TxtCity.Text;
                    break;
                case PrintType.Order:
                    _debtor.City = TxtCity.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtAtt_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.ContactPerson = TxtAtt.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.ContactPerson = TxtAtt.Text;
                    break;
                case PrintType.Order:
                    _debtor.ContactPerson = TxtAtt.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtCountry_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    //_debtor.Country = TxtCountry.Text;
                    break;
                case PrintType.Vendor:
                    //_creditor.Country = TxtCountry.Text;
                    break;
                case PrintType.Order:
                    //_debtor.Country = TxtCountry.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtDeliveryName_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.DeliveryName = TxtDeliveryName.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.DeliveryName = TxtDeliveryName.Text;
                    break;
                case PrintType.Order:
                    _debtor.DeliveryName = TxtDeliveryName.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtDeliveryAddress1_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.DeliveryAddress1 = TxtDeliveryAddress1.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.DeliveryAddress1 = TxtDeliveryAddress1.Text;
                    break;
                case PrintType.Order:
                    _debtor.DeliveryAddress1 = TxtDeliveryAddress1.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtDeliveryAddress2_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.DeliveryAddress2 = TxtDeliveryAddress2.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.DeliveryAddress2 = TxtDeliveryAddress2.Text;
                    break;
                case PrintType.Order:
                    _debtor.DeliveryAddress2 = TxtDeliveryAddress2.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtDeliveryZipCode_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.DeliveryZipCode = TxtDeliveryZipCode.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.DeliveryZipCode = TxtDeliveryZipCode.Text;
                    break;
                case PrintType.Order:
                    _debtor.DeliveryZipCode = TxtDeliveryZipCode.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtDeliveryCity_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.DeliveryCity = TxtDeliveryCity.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.DeliveryCity = TxtDeliveryCity.Text;
                    break;
                case PrintType.Order:
                    _debtor.DeliveryCity = TxtDeliveryCity.Text;
                    break;
                default:
                    break;
            }
        }

        private void TxtDeliveryAtt_TextChanged(object sender, EventArgs e)
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor.ContactPerson = TxtDeliveryAtt.Text;
                    break;
                case PrintType.Vendor:
                    _creditor.ContactPerson = TxtDeliveryAtt.Text;
                    break;
                case PrintType.Order:
                    _debtor.ContactPerson = TxtDeliveryAtt.Text;
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
