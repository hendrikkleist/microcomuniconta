﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.Pakkelabel.API
{
    public class PakkelabelUnicontaAPIHelpers
    {
        private readonly CrudAPI _crudAPI;

        public PakkelabelUnicontaAPIHelpers(CrudAPI crudAPI)
        {
            _crudAPI = crudAPI;
        }

        public async Task<DebtorOrderLineClient[]> FindLinesAsync(string orderno)
        {
            var criteria = new List<PropValuePair>();
            var whereClause = PropValuePair.GenereteWhereElements("OrderNumber", typeof(string), orderno);
            criteria.Add(whereClause);


            var output = await _crudAPI.Query(new DebtorOrderLineClient(), null, criteria);
            return output;
        }
    }
}
