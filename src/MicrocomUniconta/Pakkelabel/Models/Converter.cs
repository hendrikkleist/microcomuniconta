﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Uniconta.ClientTools.DataModel;
using Uniconta.DataModel;

namespace MicrocomUniconta.Pakkelabel.Models
{
    public class Converter
    {
        private readonly Pakkelabels _pakkelabel;
        private readonly DebtorClient _debtor;
        private readonly CreditorClient _creditor;
        private readonly DebtorOrder _debtorOrder;
        private readonly bool _isDelivery;
        private readonly PrintType _printType;
        private readonly string _services; // pakkeværdi
        private readonly string _weight;        
        private readonly DateTime _date;
        private readonly string _receiver_country;

        public Converter(
            bool IsDelivery,
            PrintType printType,
            Pakkelabels pakkelabel,
            object toPrint,
            string services,
            string weight,
            DateTime date,
            string receiver_country)
        {
            _isDelivery = IsDelivery;
            _printType = printType;
            _pakkelabel = pakkelabel;
            switch (_printType)
            {
                case PrintType.Debitor:
                    _debtor = (DebtorClient)toPrint;
                    break;
                case PrintType.Vendor:
                    _creditor = (CreditorClient)toPrint;
                    break;
                case PrintType.Order:
                    _debtorOrder = (DebtorOrder)toPrint;                    
                    break;
                default:
                    break;
            }
            
            _services = services;
            _weight = weight;
            _date = date;
            _receiver_country = receiver_country;
        }

        public Converter(
            bool IsDelivery,
            PrintType printType,
            Pakkelabels pakkelabel,
            DebtorOrder debtorOrder,
            DebtorClient debtor,
            string services,
            string weight,
            DateTime date,
            string receiver_country)
        {
            _isDelivery = IsDelivery;
            _printType = printType;
            _pakkelabel = pakkelabel;
            _debtorOrder = debtorOrder;
            _debtor = debtor;
            _services = services;
            _weight = weight;
            _date = date;
            _receiver_country = receiver_country;
        }

        public string ShipmondoOutput()
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    return ShipmondoDebtorOutput();
                case PrintType.Vendor:
                    return ShipmondoVendorOutput();
                case PrintType.Order:
                    return ShipmondoOrderOutput();
                default:
                    throw new Exception("not supposed to happen");
            }
        }

        private string ShipmondoOrderOutput()
        {
            var name = _isDelivery ? _debtor.DeliveryName : _debtor.Name;
            var address = _isDelivery ? _debtor.DeliveryAddress1 + " " + _debtor.DeliveryAddress2 : _debtor.Address1 + " " + _debtor.Address2;
            var zipCode = _isDelivery ? _debtor.DeliveryZipCode : _debtor.ZipCode;
            var city = _isDelivery ? _debtor.DeliveryCity : _debtor.City;

            string att;
            if (String.IsNullOrEmpty(_debtor.ContactPerson))
                att = _debtor.Name;
            else
                att = _debtor.ContactPerson;

            var output = ""; 
            output += "shipping_agent," +
                "shipping_product_id," +
                "services," +
                "weight," +
                "receiver_name," +
                "receiver_address1," +
                "receiver_zipcode," +
                "receiver_city," +
                "receiver_country," +
                "receiver_attention," +
                "receiver_email," +
                "sender_name," +
                "sender_address1," +
                "sender_zipcode," +
                "sender_city," +
                "sender_country," +
                "printer_name," +
                "label_format," +
                "internal_id\n";
            output += $"\"{_pakkelabel.ShippingAgent}\"," +
                $"{_pakkelabel.ShippingProductId}," +
                $"{_services}," +
                $"{_weight}," +
                $"\"{name}\"," +
                $"\"{address}\"," +
                $"\"{zipCode}\"," +
                $"\"{city}\"," +
                $"\"{_receiver_country}\"," +
                $"\"{att}\"," + // maybe something else
                $"\"{_debtor.ContactEmail}\"," +
                $"\"{_pakkelabel.SenderName}\"," +
                $"\"{_pakkelabel.SenderAddress}\"," +
                $"\"{_pakkelabel.SenderZipcode}\"," +
                $"\"{_pakkelabel.SenderCity}\"," +
                $"\"{_pakkelabel.SenderCountry}\"," +
                $"\"{_pakkelabel.PrinterName}\"," +
                $"\"{_pakkelabel.LabelFormat}\"," +
                $"{_debtorOrder._OrderNumber}";
            return output;
        }

        private string ShipmondoDebtorOutput()
        {
            var name = _isDelivery ? _debtor.DeliveryName : _debtor.Name;
            var address = _isDelivery ? _debtor.DeliveryAddress1 + " " + _debtor.DeliveryAddress2 : _debtor.Address1 + " " + _debtor.Address2;
            var zipCode = _isDelivery ? _debtor.DeliveryZipCode : _debtor.ZipCode;
            var city = _isDelivery ? _debtor.DeliveryCity : _debtor.City;
            var att = _debtor.ContactPerson;
            
            var output = "";
            output += "shipping_agent," +
                "shipping_product_id," +
                "services," +
                "weight," +
                "receiver_name," +
                "receiver_address1," +
                "receiver_zipcode," +
                "receiver_city," +
                "receiver_country," +
                "receiver_attention," +
                "receiver_email," +
                "sender_name," +
                "sender_address1," +
                "sender_zipcode," +
                "sender_city," +
                "sender_country," +
                "printer_name," +
                "label_format," +
                "internal_id\n";
            output += $"\"{_pakkelabel.ShippingAgent}\"," +
                $"{_pakkelabel.ShippingProductId}," +
                $"{_services}," +
                $"{_weight}," +
                $"\"{name}\"," +
                $"\"{address}\"," +
                $"\"{zipCode}\"," +
                $"\"{city}\"," +
                $"\"{_receiver_country}\"," +
                $"\"{att}\"," + // maybe something else
                $"\"{_debtor.ContactEmail}\"," +
                $"\"{_pakkelabel.SenderName}\"," +
                $"\"{_pakkelabel.SenderAddress}\"," +
                $"\"{_pakkelabel.SenderZipcode}\"," +
                $"\"{_pakkelabel.SenderCity}\"," +
                $"\"{_pakkelabel.SenderCountry}\"," +
                $"\"{_pakkelabel.PrinterName}\"," +
                $"\"{_pakkelabel.LabelFormat}\"," +
                $"{CleanAccount(_debtor.Account)}";
            return output;
        }

        private string ShipmondoVendorOutput()
        {
            var name = _isDelivery ? _creditor.DeliveryName : _creditor.Name;
            var address = _isDelivery ? _creditor.DeliveryAddress1 + " " + _creditor.DeliveryAddress2 : _creditor.Address1 + " " + _creditor.Address2;
            var zipCode = _isDelivery ? _creditor.DeliveryZipCode : _creditor.ZipCode;
            var city = _isDelivery ? _creditor.DeliveryCity : _creditor.City;

            var att = _creditor.ContactPerson;
            
            var output = "";
            output += "shipping_agent," +
                "shipping_product_id," +
                "services," +
                "weight," +
                "receiver_name," +
                "receiver_address1," +
                "receiver_zipcode," +
                "receiver_city," +
                "receiver_country," +
                "receiver_attention," +
                "receiver_email," +
                "sender_name," +
                "sender_address1," +
                "sender_zipcode," +
                "sender_city," +
                "sender_country," +
                "printer_name," +
                "label_format," +
                "internal_id\n";
            output += $"\"{_pakkelabel.ShippingAgent}\"," +
                $"{_pakkelabel.ShippingProductId}," +
                $"{_services}," +
                $"{_weight}," +
                $"\"{name}\"," +
                $"\"{address}\"," +
                $"\"{zipCode}\"," +
                $"\"{city}\"," +
                $"\"{_receiver_country}\"," +
                $"\"{att}\"," + // maybe something else
                $"\"{_creditor.ContactEmail}\"," +
                $"\"{_pakkelabel.SenderName}\"," +
                $"\"{_pakkelabel.SenderAddress}\"," +
                $"\"{_pakkelabel.SenderZipcode}\"," +
                $"\"{_pakkelabel.SenderCity}\"," +
                $"\"{_pakkelabel.SenderCountry}\"," +
                $"\"{_pakkelabel.PrinterName}\"," +
                $"\"{_pakkelabel.LabelFormat}\"," +
                $"{CleanAccount(_creditor.Account)}";
            return output;
        }

        public string GLSOutput()
        {
            switch (_printType)
            {
                case PrintType.Debitor:
                    return GLSDebtorOutput();
                case PrintType.Vendor:
                    return GLSVendorOutput();
                case PrintType.Order:
                    return GLSOrderOutput();
                default:
                    throw new Exception("not supposed to happen");
            }
        }

        private string GLSOrderOutput()
        {
            var orderNumber = _debtorOrder._OrderNumber;
            var consigneeName = _isDelivery ? _debtor.DeliveryName : _debtor.Name;
            var consigneeAddress1 = _isDelivery ? _debtor.DeliveryAddress1 : _debtor.Address1;
            var consigneeAddress2 = _isDelivery ? _debtor.DeliveryAddress2 : _debtor.Address2;
            var consigneeZipCode = _isDelivery ? _debtor.DeliveryZipCode : _debtor.ZipCode;
            var consigneeCity = _isDelivery ? _debtor.DeliveryCity : _debtor.City;
            var consigneeCountry = "008";
            var date = _date.ToString("dd-MM-yyyy");
            var parcelWeight = _weight;
            var numberOfParcels = "1";
            var addOnLiabilityService = _services; // value
            var parcelType = "A";
            var shipmentType = "A";
            string attention;
            if (String.IsNullOrEmpty(_debtor.ContactPerson))
                attention = _debtor.Name;
            else
                attention = _debtor.ContactPerson;
            var comment = "";
            var customerNumber = _pakkelabel.CustomerNumber;
            var consigneeMailAddress = _debtor.ContactEmail;
            var consigneePhoneNo = _debtor.Phone;
            var services = "A"; // E = Mail notification, B = "ShopReturnService, A = AddressOnlyService, C = DepositService
            var printerNo = _pakkelabel.PrinterNo;
            var parcelShopId = "";
            var altConsignorName = _pakkelabel.SenderName;
            var altConsignorAddress1 = _pakkelabel.SenderAddress;
            var altConsignorAddress2 = "";
            var altConsignorPostalCode = _pakkelabel.SenderZipcode;
            var altConsignorCity = _pakkelabel.SenderCity;
            var altConsignorCountry = "";
            var altConsignorPhoneNo = "";

            var output = 
                $"\"{orderNumber}\"," +
                $"\"{consigneeName}\"," +                   
                $"\"{consigneeAddress1}\"," +
                $"\"{consigneeAddress2}\"," +
                $"\"{consigneeZipCode}\"," +

                $"\"{consigneeCity}\"," +
                $"\"{consigneeCountry}\"," +
                $"\"{date}\"," +
                $"\"{parcelWeight}\"," +
                $"\"{numberOfParcels}\"," +

                $"\"\"," + // (Not in use - leave empty)
                $"\"{addOnLiabilityService}\"," +
                $"\"{parcelType}\"," +
                $"\"{shipmentType}\"," +
                $"\"{attention}\"," +

                $"\"{comment}\"," +
                $"\"{customerNumber}\"," +
                $"\"{consigneeMailAddress}\"," +
                $"\"{consigneePhoneNo}\"," +
                $"\"{services}\"," +

                $"\"{printerNo}\"," +
                $"\"{parcelShopId}\"," +
                $"\"{altConsignorName}\"," +
                $"\"{altConsignorAddress1}\"," +
                $"\"{altConsignorAddress2}\"," +

                $"\"{altConsignorPostalCode}\"," +
                $"\"{altConsignorCity}\"," +
                $"\"{altConsignorCountry}\"," +
                $"\"{altConsignorPhoneNo}\"";
            return output;
        }

        private string GLSDebtorOutput()
        {
            var orderNumber = CleanAccount(_debtor.Account);
            var consigneeName = _isDelivery ? _debtor.DeliveryName : _debtor.Name;
            var consigneeAddress1 = _isDelivery ? _debtor.DeliveryAddress1 : _debtor.Address1;
            var consigneeAddress2 = _isDelivery ? _debtor.DeliveryAddress2 : _debtor.Address2;
            var consigneeZipCode = _isDelivery ? _debtor.DeliveryZipCode : _debtor.ZipCode;
            var consigneeCity = _isDelivery ? _debtor.DeliveryCity : _debtor.City;
            var consigneeCountry = "008";
            var date = _date.ToString("dd-MM-yyyy");
            var parcelWeight = _weight;
            var numberOfParcels = "1";
            var addOnLiabilityService = _services; // value
            var parcelType = "A";
            var shipmentType = "A";
            string attention;
            if (String.IsNullOrEmpty(_debtor.ContactPerson))
                attention = _debtor.Name;
            else
                attention = _debtor.ContactPerson;
            var comment = "";
            var customerNumber = _pakkelabel.CustomerNumber;
            var consigneeMailAddress = _debtor.ContactEmail;
            var consigneePhoneNo = _debtor.Phone;
            var services = "A"; // E = Mail notification, B = "ShopReturnService, A = AddressOnlyService, C = DepositService
            var printerNo = _pakkelabel.PrinterNo;
            var parcelShopId = "";
            var altConsignorName = _pakkelabel.SenderName;
            var altConsignorAddress1 = _pakkelabel.SenderAddress;
            var altConsignorAddress2 = "";
            var altConsignorPostalCode = _pakkelabel.SenderZipcode;
            var altConsignorCity = _pakkelabel.SenderCity;
            var altConsignorCountry = "";
            var altConsignorPhoneNo = "";

            var output =
                $"\"{orderNumber}\"," +
                $"\"{consigneeName}\"," +
                $"\"{consigneeAddress1}\"," +
                $"\"{consigneeAddress2}\"," +
                $"\"{consigneeZipCode}\"," +

                $"\"{consigneeCity}\"," +
                $"\"{consigneeCountry}\"," +
                $"\"{date}\"," +
                $"\"{parcelWeight}\"," +
                $"\"{numberOfParcels}\"," +

                $"\"\"," + // (Not in use - leave empty)
                $"\"{addOnLiabilityService}\"," +
                $"\"{parcelType}\"," +
                $"\"{shipmentType}\"," +
                $"\"{attention}\"," +

                $"\"{comment}\"," +
                $"\"{customerNumber}\"," +
                $"\"{consigneeMailAddress}\"," +
                $"\"{consigneePhoneNo}\"," +
                $"\"{services}\"," +

                $"\"{printerNo}\"," +
                $"\"{parcelShopId}\"," +
                $"\"{altConsignorName}\"," +
                $"\"{altConsignorAddress1}\"," +
                $"\"{altConsignorAddress2}\"," +

                $"\"{altConsignorPostalCode}\"," +
                $"\"{altConsignorCity}\"," +
                $"\"{altConsignorCountry}\"," +
                $"\"{altConsignorPhoneNo}\"";
            return output;
        }

        private string GLSVendorOutput()
        {
            var orderNumber = CleanAccount(_creditor.Account);
            var consigneeName = _isDelivery ? _creditor.DeliveryName : _creditor.Name;
            var consigneeAddress1 = _isDelivery ? _creditor.DeliveryAddress1 : _creditor.Address1;
            var consigneeAddress2 = _isDelivery ? _creditor.DeliveryAddress2 : _creditor.Address2;
            var consigneeZipCode = _isDelivery ? _creditor.DeliveryZipCode : _creditor.ZipCode;
            var consigneeCity = _isDelivery ? _creditor.DeliveryCity : _creditor.City;
            var consigneeCountry = "008";
            var date = _date.ToString("dd-MM-yyyy");
            var parcelWeight = _weight;
            var numberOfParcels = "1";
            var addOnLiabilityService = _services; // value
            var parcelType = "A";
            var shipmentType = "A";
            string attention;
            if (String.IsNullOrEmpty(_creditor.ContactPerson))
                attention = _creditor.Name;
            else
                attention = _creditor.ContactPerson;
            var comment = "";
            var customerNumber = _pakkelabel.CustomerNumber;
            var consigneeMailAddress = _creditor.ContactEmail;
            var consigneePhoneNo = _creditor.Phone;
            var services = "A"; // E = Mail notification, B = "ShopReturnService, A = AddressOnlyService, C = DepositService
            var printerNo = _pakkelabel.PrinterNo;
            var parcelShopId = "";
            var altConsignorName = _pakkelabel.SenderName;
            var altConsignorAddress1 = _pakkelabel.SenderAddress;
            var altConsignorAddress2 = "";
            var altConsignorPostalCode = _pakkelabel.SenderZipcode;
            var altConsignorCity = _pakkelabel.SenderCity;
            var altConsignorCountry = "";
            var altConsignorPhoneNo = "";

            var output =
                $"\"{orderNumber}\"," +
                $"\"{consigneeName}\"," +
                $"\"{consigneeAddress1}\"," +
                $"\"{consigneeAddress2}\"," +
                $"\"{consigneeZipCode}\"," +

                $"\"{consigneeCity}\"," +
                $"\"{consigneeCountry}\"," +
                $"\"{date}\"," +
                $"\"{parcelWeight}\"," +
                $"\"{numberOfParcels}\"," +
                
                $"\"\"," + // (Not in use - leave empty)
                $"\"{addOnLiabilityService}\"," +
                $"\"{parcelType}\"," +
                $"\"{shipmentType}\"," +
                $"\"{attention}\"," +

                $"\"{comment}\"," +
                $"\"{customerNumber}\"," +
                $"\"{consigneeMailAddress}\"," +
                $"\"{consigneePhoneNo}\"," +
                $"\"{services}\"," +

                $"\"{printerNo}\"," +
                $"\"{parcelShopId}\"," +
                $"\"{altConsignorName}\"," +
                $"\"{altConsignorAddress1}\"," +
                $"\"{altConsignorAddress2}\"," +

                $"\"{altConsignorPostalCode}\"," +
                $"\"{altConsignorCity}\"," +
                $"\"{altConsignorCountry}\"," +
                $"\"{altConsignorPhoneNo}\"";
            return output;
        }

        private string CleanAccount(string account)
        {
            var output = "";
            foreach (var item in account)
            {
                if (Char.IsDigit(item))
                {
                    output += item.ToString();
                }
                else
                {
                    break;
                }
            }
            if (string.IsNullOrEmpty(output))
            {
                output = "1";
            }
            return output;
        }
    }
}
