﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.DataModel;

namespace MicrocomUniconta.Pakkelabel.Models
{


	public class Pakkelabels : TableData
	{
		public override int UserDefinedId { get { return 1322; } }

		[Display(Name = "Fil sti til eksport")]
		public string FileOutputPath
		{
			get { return this.GetUserFieldString(0); }
			set { this.SetUserFieldString(0, value); NotifyPropertyChanged("FileOutputPath"); }
		}

		[Display(Name = "Printer nummer")]
		public string PrinterNo
		{
			get { return this.GetUserFieldString(1); }
			set { this.SetUserFieldString(1, value); NotifyPropertyChanged("PrinterNo"); }
		}

		[Display(Name = "Shipping Agent")]
		public string ShippingAgent
		{
			get { return this.GetUserFieldString(2); }
			set { this.SetUserFieldString(2, value); NotifyPropertyChanged("ShippingAgent"); }
		}

		[Display(Name = "Shipping Product Id")]
		public string ShippingProductId
		{
			get { return this.GetUserFieldString(3); }
			set { this.SetUserFieldString(3, value); NotifyPropertyChanged("ShippingProductId"); }
		}

		[Display(Name = "Afsender navn")]
		public string SenderName
		{
			get { return this.GetUserFieldString(4); }
			set { this.SetUserFieldString(4, value); NotifyPropertyChanged("SenderName"); }
		}

		[Display(Name = "Afsender adresse")]
		public string SenderAddress
		{
			get { return this.GetUserFieldString(5); }
			set { this.SetUserFieldString(5, value); NotifyPropertyChanged("SenderAddress"); }
		}

		[Display(Name = "Afsender postnummer")]
		public string SenderZipcode
		{
			get { return this.GetUserFieldString(6); }
			set { this.SetUserFieldString(6, value); NotifyPropertyChanged("SenderZipcode"); }
		}

		[Display(Name = "Afsender by")]
		public string SenderCity
		{
			get { return this.GetUserFieldString(7); }
			set { this.SetUserFieldString(7, value); NotifyPropertyChanged("SenderCity"); }
		}

		[Display(Name = "Afsender land")]
		public string SenderCountry
		{
			get { return this.GetUserFieldString(8); }
			set { this.SetUserFieldString(8, value); NotifyPropertyChanged("SenderCountry"); }
		}

		[Display(Name = "Printer navn")]
		public string PrinterName
		{
			get { return this.GetUserFieldString(9); }
			set { this.SetUserFieldString(9, value); NotifyPropertyChanged("PrinterName"); }
		}

		[Display(Name = "Label Format")]
		public string LabelFormat
		{
			get { return this.GetUserFieldString(10); }
			set { this.SetUserFieldString(10, value); NotifyPropertyChanged("LabelFormat"); }
		}

		[Display(Name = "Shipping Agent")]
		public string ShippingAgentE
		{
			get { return this.GetUserFieldString(11); }
			set { this.SetUserFieldString(11, value); NotifyPropertyChanged("ShippingAgentE"); }
		}

		[Display(Name = "GLS afsender nummer")]
		public string CustomerNumber
		{
			get { return this.GetUserFieldString(11); }
			set { this.SetUserFieldString(11, value); NotifyPropertyChanged("CustomerNumber"); }
		}
	}

}
