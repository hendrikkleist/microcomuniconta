﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicrocomUniconta.Pakkelabel.Models
{
    public enum PrintType
    {
        Debitor,
        Vendor,
        Order
    }
}
