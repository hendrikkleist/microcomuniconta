﻿using MicrocomUniconta.Pakkelabel.API;
using MicrocomUniconta.Pakkelabel.Models;
using MicrocomUniconta.Pakkelabel.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;
using Uniconta.DataModel;

namespace MicrocomUniconta.Pakkelabel
{
    public class Order_Pakkelabel : IPluginBase
    {
        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "order_pakkelabel";

        public event EventHandler OnExecute;

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {
            DebtorOrderClient debtorOrder = (DebtorOrderClient)currentRow;
            var api = new PakkelabelUnicontaAPIHelpers(_crudApi);
            var task = Task.Run(async () => await api.FindLinesAsync(debtorOrder.OrderNumber.ToString()));
            task.Wait();
            debtorOrder.Lines = task.Result;

            if (debtorOrder != null)
            {
                

                var printForm = new PrintForm(_crudApi, debtorOrder, debtorOrder.Debtor, PrintType.Order);
                if (printForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    printForm.Dispose();
                    return ErrorCodes.Succes;
                }
                else
                {
                    printForm.Dispose();
                    return ErrorCodes.Succes;
                }

            }
            return ErrorCodes.Succes;
        }

        #region Uniconta Setup

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {

        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters) { }

        #endregion
    }
}
