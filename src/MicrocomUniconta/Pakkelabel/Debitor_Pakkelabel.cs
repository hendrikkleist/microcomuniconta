﻿using MicrocomUniconta.Pakkelabel.Models;
using MicrocomUniconta.Pakkelabel.Views;
using System;
using System.Collections.Generic;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;
using Uniconta.DataModel;

namespace MicrocomUniconta.Pakkelabel
{
    public class Debitor_Pakkelabel : IPluginBase
    {
        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "debitor_pakkelabel";

        public event EventHandler OnExecute;              

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {            
            DebtorClient debtor = (DebtorClient)currentRow;
            if (debtor.Account != null)
            {
                var printForm = new PrintForm(_crudApi, debtor, PrintType.Debitor);
                if (printForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    printForm.Dispose();
                    return ErrorCodes.Succes;
                }
                else
                {
                    printForm.Dispose();
                    return ErrorCodes.Succes;
                }
            }
            return ErrorCodes.Succes;
        }

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {
        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters) {}
    }
}
