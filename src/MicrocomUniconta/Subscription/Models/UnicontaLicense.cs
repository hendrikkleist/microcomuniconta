﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicrocomUniconta.Subscription.Models
{
    public class UnicontaLicense
    {
        public string CustomerNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string License { get; set; }
        public string Serialnumber { get; set; }
    }
}
