﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicrocomUniconta.Subscription.Models
{
    public class License
    {
        public string customernumber { get; set; }
        public string expirydate { get; set; }
        public string license { get; set; }
        public string serialnumber { get; set; }
    }
}
