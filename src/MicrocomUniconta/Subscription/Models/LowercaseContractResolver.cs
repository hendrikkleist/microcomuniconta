﻿using Newtonsoft.Json.Serialization;

namespace MicrocomUniconta.Subscription.Models
{
    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }
}
