﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.DataModel;

namespace MicrocomUniconta.Subscription.Models
{
	public class MicrocomSettings : TableData
	{
		public override int UserDefinedId { get { return 1326; } }

		[Display(Name = "Firebase Secret")]
		public string FirebaseAuthSecret
		{
			get { return this.GetUserFieldString(0); }
			set { this.SetUserFieldString(0, value); NotifyPropertyChanged("FirebaseAuthSecret"); }
		}

		[Display(Name = "Base URL til Firebase")]
		public string FirebaseBasePath
		{
			get { return this.GetUserFieldString(1); }
			set { this.SetUserFieldString(1, value); NotifyPropertyChanged("FirebaseBasePath"); }
		}

		[Display(Name = "Sti til log")]
		public string LogPath
		{
			get { return this.GetUserFieldString(2); }
			set { this.SetUserFieldString(2, value); NotifyPropertyChanged("LogPath"); }
		}

		[Display(Name = "Brugernavn til LicensAPI")]
		public string LicensAPIUser
		{
			get { return this.GetUserFieldString(3); }
			set { this.SetUserFieldString(3, value); NotifyPropertyChanged("LicensAPIUser"); }
		}

		[Display(Name = "Kodeord til LicensAPI")]
		public string LicensAPIPassword
		{
			get { return this.GetUserFieldString(4); }
			set { this.SetUserFieldString(4, value); NotifyPropertyChanged("LicensAPIPassword"); }
		}

		[Display(Name = "BaseUrl til SkyPOSLicense")]
		public string SkyPOSLicensBaseUrl
		{
			get { return this.GetUserFieldString(5); }
			set { this.SetUserFieldString(5, value); NotifyPropertyChanged("SkyPOSLicensBaseUrl"); }
		}
	}

}
