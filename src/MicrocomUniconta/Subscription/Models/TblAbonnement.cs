﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.Common;
using Uniconta.DataModel;

namespace MicrocomUniconta.Subscription.Models
{
	public class TblAbonnement : TableData
	{
		public override int UserDefinedId { get { return 1324; } }
		public override Type MasterType { get { return typeof(Uniconta.DataModel.Debtor); } }

		[ForeignKeyAttribute(ForeignKeyTable = typeof(InvItem))]
		[Display(Name = "Varenummer")]
		public string Vnr
		{
			get { return this.GetUserFieldString(0); }
			set { this.SetUserFieldString(0, value); NotifyPropertyChanged("Vnr"); }
		}

		[Display(Name = "Pris")]
		public double Pris
		{
			get { return this.GetUserFieldDouble(1); }
			set { this.SetUserFieldDouble(1, value); NotifyPropertyChanged("Pris"); }
		}

		[Display(Name = "Startperiode")]
		public DateTime Startper
		{
			get { return this.GetUserFieldDateTime(2); }
			set { this.SetUserFieldDateTime(2, value); NotifyPropertyChanged("Startper"); }
		}

		[Display(Name = "Servicelængde")]
		public long Servln
		{
			get { return this.GetUserFieldInt64(3); }
			set { this.SetUserFieldInt64(3, value); NotifyPropertyChanged("Servln"); }
		}

		[Display(Name = "Forud")]
		public bool Forud
		{
			get { return this.GetUserFieldBoolean(4); }
			set { this.SetUserFieldBoolean(4, value); NotifyPropertyChanged("Forud"); }
		}

		[Display(Name = "Fakperiode")]
		public string Fakperiode
		{
			get { return this.GetUserFieldString(5); }
			set { this.SetUserFieldString(5, value); NotifyPropertyChanged("Fakperiode"); }
		}

		[Display(Name = "Opsagt")]
		public bool Opsagt
		{
			get { return this.GetUserFieldBoolean(6); }
			set { this.SetUserFieldBoolean(6, value); NotifyPropertyChanged("Opsagt"); }
		}

		[Display(Name = "Serviceaftale")]
		public string Aftalenr
		{
			get { return this.GetUserFieldString(7); }
			set { this.SetUserFieldString(7, value); NotifyPropertyChanged("Aftalenr"); }
		}

		[Display(Name = "Antal")]
		public long Antal
		{
			get { return this.GetUserFieldInt64(8); }
			set { this.SetUserFieldInt64(8, value); NotifyPropertyChanged("Antal"); }
		}

		[Display(Name = "Indexkr")]
		public double Indexkr
		{
			get { return this.GetUserFieldDouble(9); }
			set { this.SetUserFieldDouble(9, value); NotifyPropertyChanged("Indexkr"); }
		}

		[Display(Name = "Indexpct")]
		public double Indexpct
		{
			get { return this.GetUserFieldDouble(10); }
			set { this.SetUserFieldDouble(10, value); NotifyPropertyChanged("Indexpct"); }
		}

		[Display(Name = "Licens")]
		public string Licens
		{
			get { return this.GetUserFieldString(11); }
			set { this.SetUserFieldString(11, value); NotifyPropertyChanged("Licens"); }
		}

		[Display(Name = "Udløbsdato")]
		public DateTime Udlb
		{
			get { return this.GetUserFieldDateTime(12); }
			set { this.SetUserFieldDateTime(12, value); NotifyPropertyChanged("Udlb"); }
		}

		[Display(Name = "Bemærkning")]
		public string Bemrk
		{
			get { return this.GetUserFieldString(13); }
			set { this.SetUserFieldString(13, value); NotifyPropertyChanged("Bemrk"); }
		}

		[Display(Name = "Serienummer")]
		public string Senr
		{
			get { return this.GetUserFieldString(14); }
			set { this.SetUserFieldString(14, value); NotifyPropertyChanged("Senr"); }
		}
	}

}
