﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicrocomUniconta.Subscription.Models
{
    public class CustomerLicense
    {
        public string SerialNumber { get; set; }
        public int CustomerNumber { get; set; }
        public string License { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int NoOfPOS { get; set; }
        public int NoOfUsers { get; set; }
        public bool MobilePay { get; set; }
        public bool Terminal { get; set; }
        public bool EmailSupport { get; set; }
        public bool PhoneSupport { get; set; }
        public bool Deleted { get; set; }
    }
}
