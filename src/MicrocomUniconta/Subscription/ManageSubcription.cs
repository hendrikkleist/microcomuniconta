﻿using MicrocomUniconta.Serialnumber.API;
using MicrocomUniconta.Subscription.API;
using MicrocomUniconta.Subscription.Models;
using MicrocomUniconta.Subscription.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.Subscription
{
    public class ManageSubcription : IPluginBase
    {
        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "manage_subscription";

        public event EventHandler OnExecute;

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {
            var subscriptionsAPI = new SubscriptionUnicontaAPIHelpers(_crudApi);
            var itemAPI = new InvItemUnicontaAPIHelpers(_crudApi);
            var serialNumbersAPI = new SerialNumberUnicontaAPIHelpers(_crudApi);

            var microcomSettingsTask = Task.Run(async () => await subscriptionsAPI.GetMicrocomSettingsAsync());
            microcomSettingsTask.Wait();
            var microcomSettings = microcomSettingsTask.Result[0];

            var itemTask = Task.Run(async () => await itemAPI.GetAllItem());
            itemTask.Wait();
            var invItemList = itemTask.Result;

            var debtor = (DebtorClient)currentRow;

            var subsForDebtorTask = Task.Run(async () => await subscriptionsAPI.GetSubscriptionsByAccountAsync(debtor.Account));
            subsForDebtorTask.Wait();
            var subscriptionsForDebtor = subsForDebtorTask.Result;

            var subsList = new List<TblAbonnement>();
            foreach (var subscription in subscriptionsForDebtor)
            {
                subsList.Add(subscription);
            }
            var manageSubscriptionForm = new ManageSubscriptionForm(_crudApi, invItemList, microcomSettings, debtor, subsList);
            manageSubscriptionForm.ShowDialog();
            manageSubscriptionForm.Dispose();
            return ErrorCodes.Succes;
        }

        #region Uniconta Setup

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {

        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters)
        {
        }

        #endregion

    }
}
