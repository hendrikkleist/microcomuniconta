﻿using MicrocomUniconta.Subscription.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.Common;

namespace MicrocomUniconta.Subscription
{
    public class GoogleLicenseAPILog : IPluginBase
    {
        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "GoogleLicenseAPILog";

        public event EventHandler OnExecute;

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {
            var api = new SubscriptionUnicontaAPIHelpers(_crudApi);
            var microcomSettingsTask = Task.Run(async () => await api.GetMicrocomSettingsAsync());
            microcomSettingsTask.Wait();
            var microcomSettings = microcomSettingsTask.Result[0];
            try
            {
                var licenseAPI = new FirebaseSubscriptionAPI(microcomSettings.FirebaseAuthSecret, microcomSettings.FirebaseBasePath, microcomSettings.LogPath);
                var logTask = Task.Run(async () => await licenseAPI.SaveLogToLocalMachineAndDeleteFromServerAsync());
                logTask.Wait();
                var result = logTask.Result;
                if (!result)
                {
                    MessageBox.Show("Fejl ved hentning/sletning af log");
                }
            }
            catch (Exception)
            {
                throw;
            }



            return ErrorCodes.Succes;
        }

        #region Uniconta Setup

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {

        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters)
        {
        }

        #endregion
    }
}
