﻿using MicrocomUniconta.Subscription.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MicrocomUniconta.Subscription.API
{
    public class SkyPOSSubscriptionAPI
    {
        private readonly string _username; // Manager
        private readonly string _password; // ManagerPassToChangeByFirstLogin
        private readonly HttpClient _httpClient;

        private APIToken token;

        public SkyPOSSubscriptionAPI(string baseAddress = "https://license.scanpos.dk:33443", string username = "c5", string password = "!REDewZFRv$74w7TmQGs")
        {
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;            
            _username = username;
            _password = password;
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(baseAddress);
        }

        public async Task AuthorizeAsync()
        {                        
            _httpClient.DefaultRequestHeaders.Clear();
            
            var authenticationString = $"{_username}:{_password}";
            var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "/api/v1/connectivity");
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

            try
            {
                var result = await _httpClient.SendAsync(requestMessage);
                if (result.StatusCode == HttpStatusCode.OK)
                {
                    token = JsonConvert.DeserializeObject<APIToken>(await result.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<CustomerLicense> NewOrUpdateSubscription(int customerNumber, string license, string serialNumber, DateTime expiryDate, 
            int noOfPOS, int noOfUsers, bool mobilePay, bool terminal, bool emailSupport, bool phoneSupport, bool deleted)
        {
            _httpClient.DefaultRequestHeaders.Clear();            
            var licenseObj = GetLicense(customerNumber, license, serialNumber, expiryDate, noOfPOS, noOfUsers, mobilePay, terminal, emailSupport, phoneSupport, deleted);
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "/api/v1/customerlicense");
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);
            var content = JsonConvert.SerializeObject(licenseObj, Formatting.None, 
                new JsonSerializerSettings
                { 
                    ContractResolver = new LowercaseContractResolver() 
                });

            requestMessage.Content = new StringContent(content);            
            requestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            try
            {
                var result = await _httpClient.SendAsync(requestMessage);
                if (result.StatusCode != HttpStatusCode.OK)
                {
                    return null;
                }
                var output = JsonConvert.DeserializeObject<CustomerLicense>(await result.Content.ReadAsStringAsync());
                return output;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private CustomerLicense GetLicense(int customerNumber, string license, string serialNumber, DateTime expiryDate, int noOfPOS, int noOfUsers, bool mobilePay, bool terminal, bool emailSupport, bool phoneSupport, bool deleted)
        {
            return new CustomerLicense()
            {
                CustomerNumber = customerNumber,
                License = license,
                SerialNumber = serialNumber,
                ExpiryDate = expiryDate,
                NoOfPOS = noOfPOS,
                NoOfUsers = noOfUsers,
                MobilePay = mobilePay,
                Terminal = terminal,
                EmailSupport = emailSupport,
                PhoneSupport = phoneSupport,
                Deleted = deleted
            };
        }
    }
}
