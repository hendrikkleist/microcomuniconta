﻿using MicrocomUniconta.Subscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.System;
using Uniconta.Common;

namespace MicrocomUniconta.Subscription.API
{
    public class SubscriptionUnicontaAPIHelpers
    {
        private readonly CrudAPI _crudAPI;

        public SubscriptionUnicontaAPIHelpers(CrudAPI crudAPI)
        {
            _crudAPI = crudAPI;
        }

        public async Task<MicrocomSettings[]> GetMicrocomSettingsAsync()
        {
            var output = await _crudAPI.Query(new MicrocomSettings(), null, null);
            return output;
        }

        public async Task<TblAbonnement[]> GetSubscriptionsByAccountAsync(string account)
        {
            var criteria = new List<PropValuePair>();
            var whereClause = PropValuePair.GenereteWhereElements("MasterKey", typeof(string), account);
            criteria.Add(whereClause);
            var output = await _crudAPI.Query(new TblAbonnement(), null, criteria);
            return output;
        }

        public async Task<TblAbonnement[]> GetAllSubscriptionsAsync()
        {
            var output = await _crudAPI.Query(new TblAbonnement(), null, null);
            return output;
        }

        public async Task<ErrorCodes> UpdateSubscription(TblAbonnement subscription)
        {
            return await _crudAPI.Update(subscription);
        }

        public async Task<ErrorCodes> AddSubscription(TblAbonnement subscription)
        {
            return await _crudAPI.Insert(subscription);
        }
    }
}
