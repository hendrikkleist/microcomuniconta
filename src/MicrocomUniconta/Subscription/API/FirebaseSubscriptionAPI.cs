﻿using MicrocomUniconta.FireSharp;
using MicrocomUniconta.FireSharp.Config;
using MicrocomUniconta.FireSharp.Interfaces;
using MicrocomUniconta.FireSharp.Response;
using MicrocomUniconta.Subscription.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MicrocomUniconta.Subscription.API
{
    public class FirebaseSubscriptionAPI
    {
        private readonly FirebaseClient _client;
        private readonly string _logPath;

        public FirebaseSubscriptionAPI(string authSecret, string basePath, string logPath)
        {
            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = authSecret,
                BasePath = basePath,                
            };
            _client = new FirebaseClient(config);
            _logPath = logPath;
        }

        public async Task<Models.License> InsertOrUpdateSubscriptionAsync(Models.License license)
        {
            SetResponse response = await _client.SetAsync($"license/{license.serialnumber}", license);
            var result = response.ResultAs<Models.License>();
            return result;
        }

        public async Task<bool> DeleteLicenseAsync(Models.License license)
        {
            FirebaseResponse response = await _client.DeleteAsync($"license/{license.serialnumber}");
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return true;
            else
                return false;
        }

        public async Task<bool> SaveLogToLocalMachineAndDeleteFromServerAsync()
        {            
            FirebaseResponse response = await _client.GetAsync("licenselog");
            var path = $"{_logPath}\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var location = $"{_logPath}\\Logs\\Log {DateTime.Now:yyyy-MM-dd HH mm ss}.txt";
            File.WriteAllText(location, response.Body);
            FirebaseResponse deleteResponse = await _client.DeleteAsync("licenselog");
            var output = false;
            if (deleteResponse.StatusCode == HttpStatusCode.OK)
                output = true;
            return output;
        }

        public async Task<IEnumerable<UnicontaLicense>> GetAllSubscriptionsAsync()
        {            
            FirebaseResponse response = await _client.GetAsync("license");
            List<UnicontaLicense> licenses = new List<UnicontaLicense>();
            dynamic dynamicLicenses = JsonConvert.DeserializeObject<dynamic>(response.Body);
            foreach (var dynamicLicense in dynamicLicenses)
            {
                foreach (var item in dynamicLicense)
                {
                    string expiryDateStr = item.expirydate;
                    DateTime expiryDate = DateTime.ParseExact(expiryDateStr, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    var licenseObject = new UnicontaLicense()
                    {
                        CustomerNumber = item.customernumber,
                        License = item.license,
                        ExpiryDate = expiryDate,
                        Serialnumber = item.serialnumber
                    };
                    var nullLicense = string.IsNullOrEmpty(licenseObject.License);
                    var validExpiryDate = licenseObject.ExpiryDate > DateTime.Now;
                    if (!nullLicense && validExpiryDate)
                        licenses.Add(licenseObject);
                }
            }
            return licenses;
        }

    }
}
