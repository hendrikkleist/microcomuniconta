﻿using MicrocomUniconta.Helper;
using MicrocomUniconta.Serialnumber.Models;
using MicrocomUniconta.Subscription.API;
using MicrocomUniconta.Subscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;

namespace MicrocomUniconta.Subscription.Views
{
    public partial class ManageSubscriptionForm : Form
    {        
        private readonly SubscriptionUnicontaAPIHelpers _subscriptionsAPI;
        private readonly IEnumerable<InvItemClient> _invItems;
        private readonly DebtorClient _debtor;
        private readonly FirebaseSubscriptionAPI _licenseAPI;        
        private readonly SkyPOSSubscriptionAPI _skyPOSSubscriptionAPI;

        private bool _isNewSub = false;
        private bool _isArrowDown = false;

        private bool _isLicense = false;

        private ListViewEx<TblAbonnement> _lvSubscriptions;
        private TblAbonnement _subscription;

        public ManageSubscriptionForm(CrudAPI crudApi, IEnumerable<InvItemClient> invItems, MicrocomSettings settings, DebtorClient debtor, List<TblAbonnement> subs)
        {
            InitializeComponent();
            _invItems = invItems;
            _debtor = debtor;
            Text += $" (Konto: {debtor.Account} - {debtor.Name})";
            _subscriptionsAPI = new SubscriptionUnicontaAPIHelpers(crudApi);            
            try
            {
                _licenseAPI = new FirebaseSubscriptionAPI(settings.FirebaseAuthSecret, settings.FirebaseBasePath, settings.LogPath);
            }
            catch (Exception)
            {
                throw;
            }
            _skyPOSSubscriptionAPI = new SkyPOSSubscriptionAPI(settings.SkyPOSLicensBaseUrl, settings.LicensAPIUser, settings.LicensAPIPassword);
            var authenticateTask = Task.Run(async () => await _skyPOSSubscriptionAPI.AuthorizeAsync());
            authenticateTask.Wait();         

            SetupInvItemCombobox(invItems);

            SetupListView(subs);                                      
        }        

        private void SetupInvItemCombobox(IEnumerable<InvItemClient> invItems)
        {            
            CbInvItem.Items.Add("");
            foreach (var invItem in invItems)
            {
                var invItemItem = new CustomComboboxItem()
                {
                    Text = $"{invItem.Item} - {invItem.Name}",
                    Value = invItem
                };
                CbInvItem.Items.Add(invItemItem);
            }            
        }


        /// <summary>
        /// Creates a listview and attaches it to a control
        /// </summary>
        /// <param name="subscriptions"></param>
        private void SetupListView(List<TblAbonnement> subscriptions)
        {
            GboxTitle.Controls.Remove(_lvSubscriptions);
            var columnMapping = new List<(string ColumnName, Func<TblAbonnement, object> ValueLookup, Func<TblAbonnement, string> DisplayStringLookup)>()
            {
                ("Varenummer", s => s.Vnr, s => s.Vnr),
                ("Pris", s => s.Pris, s => s.Pris.ToString()),
                ("Startperiode", s => s.Startper, s => $"{s.Startper:dd MMM yyyy}"),
                ("Servicelængde", s => s.Servln, s => s.Servln.ToString()),
                ("Forud", s => s.Forud, s => s.Forud ? "Ja" : "Nej"),
                ("Fakperiode", s => s.Fakperiode, s => s.Fakperiode),
                ("Udløbsdato", s => s.Udlb, s => $"{s.Udlb:dd MMM yyyy}"),
                ("Opsagt", s => s.Opsagt, s => s.Opsagt ? "Ja" : "Nej"),
                //("Serviceaftale", s => s.Aftalenr, s => s.Aftalenr),
                //("Antal", s => s.Antal, s => s.Antal.ToString()),
                //("Indexkr", s => s.Indexkr, s => s.Indexkr.ToString()),
                //("Indexpct", s => s.Indexpct, s => s.Indexpct.ToString()),
                ("Serienummer", s => s.Senr, s => s.Senr),
                ("Licens", s => s.Licens, s => s.Licens),
                ("Bemærkning", s => s.Bemrk, s => s.Bemrk),
            };

            _lvSubscriptions = new ListViewEx<TblAbonnement>(columnMapping)
            {
                FullRowSelect = true,
                MultiSelect = false,
                View = View.Details,
                Left = 20,
                Top = 20,
                Width = 500,
                Height = 300,
                Dock = DockStyle.Fill
            };

            _lvSubscriptions.SelectedIndexChanged += LvSubscriptions_SelectedIndexChanged;
            _lvSubscriptions.KeyDown += _lvSubscriptions_KeyDown;
            

            _lvSubscriptions.AddRange(subscriptions);
            if (subscriptions.Count > 0)
            {
                _subscription = subscriptions[0];
                SetFields();
                ChangeButtonState();
            }
            else
            {
                UpdateBTN.Text = "Opret";
                _isNewSub = true;
            }
            GboxTitle.Controls.Add(_lvSubscriptions);

        }

        private void _lvSubscriptions_KeyDown(object sender, KeyEventArgs e)
        {
            var keyDown = e.KeyCode == Keys.Down;
            var last = _lvSubscriptions.Items.Count == _lvSubscriptions.FocusedItem?.Index + 1 ? true : false;
            if (keyDown && last)
            {
                _isArrowDown = true;
                if (!string.IsNullOrEmpty(_subscription?.Vnr))
                { 
                    ResetAndPrimeToNewSubscription();
                }
            }   
            if (e.KeyCode != Keys.Down)
            {
                _isArrowDown = false;
            }
        }


        /// <summary>
        /// Eventhandler for changing the fields.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvSubscriptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lvSubscriptions.SelectedItems.Count > 0)
            {
                var numOfItems = _lvSubscriptions.Items.Count;
                if (_lvSubscriptions.FocusedItem.Index > -1)
                {
                    if (_isNewSub)
                    {
                        // Remove the last item
                        for (int i = 0; i < _lvSubscriptions.Items.Count + 1; i++)
                        {
                            if ((i == _lvSubscriptions.Items.Count) && string.IsNullOrEmpty(_subscription?.Vnr) && !_isArrowDown)
                            {
                                _lvSubscriptions.Items.RemoveAt(i-1);
                                _isNewSub = false;
                            }
                        }
                    }
                    _subscription = _lvSubscriptions.FocusedItem.Tag as TblAbonnement;
                    SetFields();
                }
            }
        }

        /// <summary>
        /// Takes Uniconta subscription model and puts values to the editing screen.
        /// </summary>
        private void SetFields()
        {
            TxtPrice.Text = _subscription.Pris.ToString();
            if (_subscription.Startper == DateTime.MinValue)
            {
                DtpStartTerm.Value = DateTime.Now;
            }
            else
            {
                DtpStartTerm.Value = _subscription.Startper;
            }

            CbxPrepaid.Checked = _subscription.Forud;
            switch (_subscription.Fakperiode)
            {
                case "Måned":
                    RbtnMonthly.Checked = true;
                    break;
                case "Kvartal":
                    RbtnQuarterly.Checked = true;
                    break;
                case "Halvår":
                    RbtnBiannually.Checked = true;
                    break;
                case "År":
                    RbtnYearly.Checked = true;
                    break;                
            }

            CbxTerminated.Checked = _subscription.Opsagt;
            TxtServiceAgreement.Text = _subscription.Aftalenr;
            TxtAmount.Text = _subscription.Antal.ToString();
            TxtIndexPrice.Text = _subscription.Indexkr.ToString();
            TxtIndexPercent.Text = _subscription.Indexpct.ToString();
            TxtLicense.Text = _subscription.Licens;
            TxtSerialNumber.Text = _subscription.Senr;
            if (_subscription.Udlb == DateTime.MinValue)
            {
                DtpExpiryDate.Value = DateTime.Now;
            }
            else
            {
                DtpExpiryDate.Value = _subscription.Udlb;
            }
            
            TxtRemark.Text = _subscription.Bemrk;

            if (!String.IsNullOrEmpty(_subscription.Vnr))
            {
                for (int i = 1; i < CbInvItem.Items.Count; i++)
                {
                    CbInvItem.SelectedIndex = i;
                    var customCbItem = CbInvItem.SelectedItem as CustomComboboxItem;
                    var item = customCbItem.Value as InvItemClient;
                    if (item.Item == _subscription.Vnr)
                    {
                        lblItem.Text = _subscription.Vnr;

                        var itemName = _invItems.First(s => s.Item == _subscription.Vnr).Name;
                        lblItem.Text = $"{_subscription.Vnr} - {itemName}";
                        break;
                    }
                }
            }

            if ((_subscription.Licens != null))
            {
                UpdateBTN.Text = "Opdater og gem licens";
                _isLicense = true;
            }
            else if (!_isNewSub)
            {
                UpdateBTN.Text = "Opdater abonnement";
                _isLicense = false;
            }
            else
            {
                UpdateBTN.Text = "Opret abonnement";
            }

        }

        /// <summary>
        /// Update Firebase, Uniconta and eventually self hosted License DB using API.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void UpdateBTN_Click(object sender, EventArgs e)
        {
            if (_isNewSub || _lvSubscriptions.Items.Count == 0)
            {
                _subscription = GetUnicontaSubscription();
                await _subscriptionsAPI.AddSubscription(_subscription);
                _isNewSub = false;
                UpdateBTN.Text = "Opdater abonnement";
            }
            else
            {
                var unicontaSubscription = GetUnicontaSubscription();
                await _subscriptionsAPI.UpdateSubscription(unicontaSubscription);
                _lvSubscriptions.Refresh();
            }

            if (_isLicense)
            {
                var firebaseSubscription = GetFirebaseSubscription();
                await _licenseAPI.InsertOrUpdateSubscriptionAsync(firebaseSubscription);
                await _skyPOSSubscriptionAPI.NewOrUpdateSubscription(int.Parse(_debtor.Account),
                TxtLicense.Text, TxtSerialNumber.Text, DtpExpiryDate.Value, 10, 10,
                    true, true, true, true, CbxTerminated.Checked);
            }
            var subs = await _subscriptionsAPI.GetSubscriptionsByAccountAsync(_debtor.Account);
            var subsList = new List<TblAbonnement>();
            foreach (var subscription in subs)
            {
                subsList.Add(subscription);
            }
            _lvSubscriptions.SelectedIndexChanged -= LvSubscriptions_SelectedIndexChanged;
            SetupListView(subsList);
        }

        /// <summary>
        /// Assigning Values from screen to Firebase subscription model
        /// </summary>
        /// <returns></returns>
        private Models.License GetFirebaseSubscription()
        {
            return new Models.License()
            {
            customernumber = _debtor.Account,
                expirydate = DtpExpiryDate.Value.ToString("dd-MM-yyyy"),
                license = TxtLicense.Text,
                serialnumber = TxtSerialNumber.Text
            };
        }

        /// <summary>
        /// Assigning Values from screen to Uniconta subscription model
        /// </summary>
        /// <returns><see cref="TblAbonnement"/></returns>
        private TblAbonnement GetUnicontaSubscription()
        {
            TblAbonnement output;
            if (_isNewSub)
            {
                output = new TblAbonnement();
                output.SetMaster(_debtor);
            }
            else
            {
                output = _subscription;
            }
            if (!string.IsNullOrEmpty(TxtPrice.Text))
                output.Pris = Double.Parse(TxtPrice.Text);
            else
                output.Pris = 0;
            output.Startper = DtpStartTerm.Value;
            output.Forud = CbxPrepaid.Checked;
            if (RbtnMonthly.Checked)
                output.Fakperiode = "Måned";
            if (RbtnQuarterly.Checked)
            output.Fakperiode = "Kvartal";
            if (RbtnBiannually.Checked)
                output.Fakperiode = "Halvår";
            if (RbtnYearly.Checked)
                output.Fakperiode = "År";
            output.Opsagt = CbxTerminated.Checked;
            output.Aftalenr = TxtServiceAgreement.Text;
            if (!string.IsNullOrEmpty(TxtAmount.Text))
            {
                output.Antal = Int64.Parse(TxtAmount.Text);                
            }
            else
            {
                output.Antal = 1;
            }
            if (!string.IsNullOrEmpty(TxtIndexPrice.Text))
            {
                output.Indexkr = Double.Parse(TxtIndexPrice.Text);
            }
            else
            {
                output.Indexkr = 0;
            }
            if (!string.IsNullOrEmpty(TxtIndexPercent.Text))
            {
                output.Indexpct = Double.Parse(TxtIndexPercent.Text);
            }
            else
            {
                output.Indexpct = 0;
            }
            output.Licens = TxtLicense.Text;
            output.Senr = TxtSerialNumber.Text;
            output.Udlb = DtpExpiryDate.Value;
            output.Bemrk = TxtRemark.Text;
            var customCbItem = CbInvItem.SelectedItem as CustomComboboxItem;
            if (customCbItem != null)
            {
                var item = customCbItem.Value as InvItemClient;
                output.Vnr = item.Item;
            }

            return output;
        }



        private void TxtLicense_TextChanged(object sender, EventArgs e)
        {
            ChangeButtonState();
        }

        private void ChangeButtonState()
        {
            if ((!String.IsNullOrEmpty(TxtLicense.Text)) && (!String.IsNullOrEmpty(TxtSerialNumber.Text)))
            {
                UpdateBTN.Text = "Opdater og gem licens";
                _isLicense = true;
            }
            else
            {
                if (_isNewSub)
                {
                    UpdateBTN.Text = "Opret abonnement";
                }
                else
                {
                    UpdateBTN.Text = "Opdater abonnement";
                }
                _isLicense = false;
            }
        }

        private void TxtSerialNumber_TextChanged(object sender, EventArgs e)
        {
            if ((!String.IsNullOrEmpty(TxtLicense.Text)) && (!String.IsNullOrEmpty(TxtSerialNumber.Text)))
            {
                UpdateBTN.Text = "Opdater og gem licens";
                _isLicense = true;
            }
            else
            {
                if (_isNewSub)
                {
                    UpdateBTN.Text = "Opret abonnement";
                }
                else
                {
                    UpdateBTN.Text = "Opdater abonnement";
                }
                _isLicense = false;
            }
        }
        private void ResetAndPrimeToNewSubscription()
        {
            _isNewSub = true;
            CbInvItem.SelectedIndex = 0;
            CbxPrepaid.Checked = false;
            CbxTerminated.Checked = false;
            TxtAmount.Text = "1";
            TxtIndexPrice.Text = "";
            TxtIndexPercent.Text = "";
            TxtPrice.Text = "";
            TxtRemark.Text = "";
            TxtLicense.Text = "";
            TxtSerialNumber.Text = "";
            DtpStartTerm.Value = DateTime.Now;
            DtpExpiryDate.Value = DateTime.Now;
            TxtServiceAgreement.Text = "";            
            _subscription = new TblAbonnement();
            _subscription.SetMaster(_debtor);
            _lvSubscriptions.Add(_subscription);
        }
    }
}
