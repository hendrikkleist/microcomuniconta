﻿
namespace MicrocomUniconta.Subscription.Views
{
    partial class ManageSubscriptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GboxTitle = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.CbInvItem = new System.Windows.Forms.ComboBox();
            this.UpdateBTN = new System.Windows.Forms.Button();
            this.TxtRemark = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.DtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtSerialNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtLicense = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtIndexPercent = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtIndexPrice = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtAmount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtServiceAgreement = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CbxTerminated = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RbtnYearly = new System.Windows.Forms.RadioButton();
            this.RbtnBiannually = new System.Windows.Forms.RadioButton();
            this.RbtnQuarterly = new System.Windows.Forms.RadioButton();
            this.RbtnMonthly = new System.Windows.Forms.RadioButton();
            this.CbxPrepaid = new System.Windows.Forms.CheckBox();
            this.DtpStartTerm = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblItem = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // GboxTitle
            // 
            this.GboxTitle.Location = new System.Drawing.Point(18, 38);
            this.GboxTitle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GboxTitle.Name = "GboxTitle";
            this.GboxTitle.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GboxTitle.Size = new System.Drawing.Size(1570, 500);
            this.GboxTitle.TabIndex = 2;
            this.GboxTitle.TabStop = false;
            this.GboxTitle.Text = "Data";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.CbInvItem);
            this.groupBox1.Controls.Add(this.UpdateBTN);
            this.groupBox1.Controls.Add(this.TxtRemark);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.DtpExpiryDate);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.TxtSerialNumber);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.TxtLicense);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TxtIndexPercent);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TxtIndexPrice);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TxtAmount);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtServiceAgreement);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.CbxTerminated);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.CbxPrepaid);
            this.groupBox1.Controls.Add(this.DtpStartTerm);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtPrice);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(18, 548);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1300, 335);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Felter";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 42);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 20);
            this.label11.TabIndex = 23;
            this.label11.Text = "Vare";
            // 
            // CbInvItem
            // 
            this.CbInvItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.CbInvItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CbInvItem.FormattingEnabled = true;
            this.CbInvItem.Location = new System.Drawing.Point(8, 71);
            this.CbInvItem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CbInvItem.Name = "CbInvItem";
            this.CbInvItem.Size = new System.Drawing.Size(348, 28);
            this.CbInvItem.TabIndex = 0;
            // 
            // UpdateBTN
            // 
            this.UpdateBTN.Location = new System.Drawing.Point(8, 117);
            this.UpdateBTN.Name = "UpdateBTN";
            this.UpdateBTN.Size = new System.Drawing.Size(350, 83);
            this.UpdateBTN.TabIndex = 14;
            this.UpdateBTN.Text = "Opdater og gem licens";
            this.UpdateBTN.UseVisualStyleBackColor = true;
            this.UpdateBTN.Click += new System.EventHandler(this.UpdateBTN_Click);
            // 
            // TxtRemark
            // 
            this.TxtRemark.Location = new System.Drawing.Point(548, 275);
            this.TxtRemark.Name = "TxtRemark";
            this.TxtRemark.Size = new System.Drawing.Size(349, 26);
            this.TxtRemark.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(544, 252);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 20);
            this.label10.TabIndex = 21;
            this.label10.Text = "Bemærkning";
            // 
            // DtpExpiryDate
            // 
            this.DtpExpiryDate.Location = new System.Drawing.Point(908, 275);
            this.DtpExpiryDate.Name = "DtpExpiryDate";
            this.DtpExpiryDate.Size = new System.Drawing.Size(349, 26);
            this.DtpExpiryDate.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(903, 252);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Udløbsdato";
            // 
            // TxtSerialNumber
            // 
            this.TxtSerialNumber.Location = new System.Drawing.Point(908, 117);
            this.TxtSerialNumber.Name = "TxtSerialNumber";
            this.TxtSerialNumber.Size = new System.Drawing.Size(349, 26);
            this.TxtSerialNumber.TabIndex = 10;
            this.TxtSerialNumber.TextChanged += new System.EventHandler(this.TxtSerialNumber_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(903, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "Serienummer";
            // 
            // TxtLicense
            // 
            this.TxtLicense.Location = new System.Drawing.Point(908, 65);
            this.TxtLicense.Name = "TxtLicense";
            this.TxtLicense.Size = new System.Drawing.Size(349, 26);
            this.TxtLicense.TabIndex = 9;
            this.TxtLicense.TextChanged += new System.EventHandler(this.TxtLicense_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(903, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "Licens";
            // 
            // TxtIndexPercent
            // 
            this.TxtIndexPercent.Location = new System.Drawing.Point(548, 172);
            this.TxtIndexPercent.Name = "TxtIndexPercent";
            this.TxtIndexPercent.Size = new System.Drawing.Size(349, 26);
            this.TxtIndexPercent.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(544, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Index Procent";
            // 
            // TxtIndexPrice
            // 
            this.TxtIndexPrice.Location = new System.Drawing.Point(548, 117);
            this.TxtIndexPrice.Name = "TxtIndexPrice";
            this.TxtIndexPrice.Size = new System.Drawing.Size(349, 26);
            this.TxtIndexPrice.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(544, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Index Kroner";
            // 
            // TxtAmount
            // 
            this.TxtAmount.Location = new System.Drawing.Point(548, 65);
            this.TxtAmount.Name = "TxtAmount";
            this.TxtAmount.Size = new System.Drawing.Size(349, 26);
            this.TxtAmount.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(544, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Antal";
            // 
            // TxtServiceAgreement
            // 
            this.TxtServiceAgreement.Location = new System.Drawing.Point(908, 225);
            this.TxtServiceAgreement.Name = "TxtServiceAgreement";
            this.TxtServiceAgreement.Size = new System.Drawing.Size(349, 26);
            this.TxtServiceAgreement.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(903, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Service aftale";
            // 
            // CbxTerminated
            // 
            this.CbxTerminated.AutoSize = true;
            this.CbxTerminated.Location = new System.Drawing.Point(366, 240);
            this.CbxTerminated.Name = "CbxTerminated";
            this.CbxTerminated.Size = new System.Drawing.Size(87, 24);
            this.CbxTerminated.TabIndex = 3;
            this.CbxTerminated.Text = "Opsagt";
            this.CbxTerminated.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RbtnYearly);
            this.groupBox2.Controls.Add(this.RbtnBiannually);
            this.groupBox2.Controls.Add(this.RbtnQuarterly);
            this.groupBox2.Controls.Add(this.RbtnMonthly);
            this.groupBox2.Location = new System.Drawing.Point(366, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 158);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Fakturering periode";
            // 
            // RbtnYearly
            // 
            this.RbtnYearly.AutoSize = true;
            this.RbtnYearly.Location = new System.Drawing.Point(10, 115);
            this.RbtnYearly.Name = "RbtnYearly";
            this.RbtnYearly.Size = new System.Drawing.Size(50, 24);
            this.RbtnYearly.TabIndex = 3;
            this.RbtnYearly.TabStop = true;
            this.RbtnYearly.Text = "År";
            this.RbtnYearly.UseVisualStyleBackColor = true;
            // 
            // RbtnBiannually
            // 
            this.RbtnBiannually.AutoSize = true;
            this.RbtnBiannually.Location = new System.Drawing.Point(10, 85);
            this.RbtnBiannually.Name = "RbtnBiannually";
            this.RbtnBiannually.Size = new System.Drawing.Size(79, 24);
            this.RbtnBiannually.TabIndex = 2;
            this.RbtnBiannually.TabStop = true;
            this.RbtnBiannually.Text = "Halvår";
            this.RbtnBiannually.UseVisualStyleBackColor = true;
            // 
            // RbtnQuarterly
            // 
            this.RbtnQuarterly.AutoSize = true;
            this.RbtnQuarterly.Location = new System.Drawing.Point(10, 55);
            this.RbtnQuarterly.Name = "RbtnQuarterly";
            this.RbtnQuarterly.Size = new System.Drawing.Size(82, 24);
            this.RbtnQuarterly.TabIndex = 1;
            this.RbtnQuarterly.TabStop = true;
            this.RbtnQuarterly.Text = "Kvartal";
            this.RbtnQuarterly.UseVisualStyleBackColor = true;
            // 
            // RbtnMonthly
            // 
            this.RbtnMonthly.AutoSize = true;
            this.RbtnMonthly.Location = new System.Drawing.Point(10, 25);
            this.RbtnMonthly.Name = "RbtnMonthly";
            this.RbtnMonthly.Size = new System.Drawing.Size(83, 24);
            this.RbtnMonthly.TabIndex = 0;
            this.RbtnMonthly.TabStop = true;
            this.RbtnMonthly.Text = "Måned";
            this.RbtnMonthly.UseVisualStyleBackColor = true;
            // 
            // CbxPrepaid
            // 
            this.CbxPrepaid.AutoSize = true;
            this.CbxPrepaid.Location = new System.Drawing.Point(366, 208);
            this.CbxPrepaid.Name = "CbxPrepaid";
            this.CbxPrepaid.Size = new System.Drawing.Size(77, 24);
            this.CbxPrepaid.TabIndex = 2;
            this.CbxPrepaid.Text = "Forud";
            this.CbxPrepaid.UseVisualStyleBackColor = true;
            // 
            // DtpStartTerm
            // 
            this.DtpStartTerm.Location = new System.Drawing.Point(908, 172);
            this.DtpStartTerm.Name = "DtpStartTerm";
            this.DtpStartTerm.Size = new System.Drawing.Size(349, 26);
            this.DtpStartTerm.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(903, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Start Periode";
            // 
            // TxtPrice
            // 
            this.TxtPrice.Location = new System.Drawing.Point(548, 225);
            this.TxtPrice.Name = "TxtPrice";
            this.TxtPrice.Size = new System.Drawing.Size(349, 26);
            this.TxtPrice.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(544, 202);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pris";
            // 
            // lblItem
            // 
            this.lblItem.AutoSize = true;
            this.lblItem.Location = new System.Drawing.Point(16, 14);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(0, 20);
            this.lblItem.TabIndex = 10;
            // 
            // ManageSubscriptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1606, 918);
            this.Controls.Add(this.lblItem);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GboxTitle);
            this.Name = "ManageSubscriptionForm";
            this.Text = "Abonnementer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GboxTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker DtpStartTerm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton RbtnYearly;
        private System.Windows.Forms.RadioButton RbtnBiannually;
        private System.Windows.Forms.RadioButton RbtnQuarterly;
        private System.Windows.Forms.RadioButton RbtnMonthly;
        private System.Windows.Forms.CheckBox CbxPrepaid;
        private System.Windows.Forms.TextBox TxtAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtServiceAgreement;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox CbxTerminated;
        private System.Windows.Forms.TextBox TxtRemark;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker DtpExpiryDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtSerialNumber;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtLicense;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtIndexPercent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtIndexPrice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button UpdateBTN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CbInvItem;
        private System.Windows.Forms.Label lblItem;
    }
}