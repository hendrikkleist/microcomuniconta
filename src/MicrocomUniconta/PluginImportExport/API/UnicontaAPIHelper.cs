﻿using MicrocomUniconta.PluginImportExport.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.System;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;

namespace MicrocomUniconta.PluginImportExport.API
{   
    public class UnicontaAPIHelper
    {
        private readonly CrudAPI _crudAPI;
        private readonly string _filename = @"C:\Uniconta\PluginPath\plugins.json"; 

        public UnicontaAPIHelper(CrudAPI crudAPI)
        {
            _crudAPI = crudAPI;
        }

        public bool ExportPlugin(IEnumerable<UnicontaBaseEntity> source)
        {
            var plugins = new List<PluginModel>();
            foreach (var item in source)
            {
                var plugin = item as UserPluginClient;
                plugins.Add(new PluginModel()
                { 
                    ClassName = plugin.ClassName,
                    Command = plugin.Command,
                    Control = plugin.Control,
                    Dll = plugin.Dll,
                    Name = plugin.Name,
                    Prompt = plugin.Prompt
                });
            }
            var pluginJson = JsonConvert.SerializeObject(plugins);
            using (var fs = File.Create(_filename))
            {
                AddText(fs, pluginJson);
            }
            if (!File.Exists(_filename))
                return false;
            return true;
        }

        public async Task<bool> ImportPlugin(IEnumerable<UnicontaBaseEntity> source)
        {
            var file = File.ReadAllText(_filename);
            var plugins = JsonConvert.DeserializeObject<List<PluginModel>>(file);
            var pluginClients = new List<UserPluginClient>();
            foreach (var item in plugins)
            {
                var found = false;
                foreach (var existingItem in source)
                {
                    var plugin = existingItem as UserPluginClient;
                    if ((plugin.ClassName == item.ClassName) && (plugin.Command == item.Command))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    pluginClients.Add(new UserPluginClient()
                    {
                        ClassName = item.ClassName,
                        Command = item.Command,
                        Control = item.Control,
                        Dll = item.Dll,
                        Name = item.Name,
                        Prompt = item.Prompt
                    });
                }
            }
            var insert = await _crudAPI.Insert(pluginClients);
            if (insert != ErrorCodes.Succes)
                return false;
            return true;    
        }

        private static void AddText(FileStream fs, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            fs.Write(info, 0, info.Length);
        }
    }
}
