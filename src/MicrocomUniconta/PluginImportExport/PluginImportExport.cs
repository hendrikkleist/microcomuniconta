﻿using MicrocomUniconta.PluginImportExport.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.Common;

namespace MicrocomUniconta.PluginImportExport
{
    public class PluginImportExport : IPluginBase
    {
        private BaseAPI _baseApi;
        private CrudAPI _crudApi;

        public string Name => "manage_plugin";

        public event EventHandler OnExecute;

        public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
        {
            var api = new UnicontaAPIHelper(_crudApi);

            if (command == "export")
            {
                var eksport = api.ExportPlugin(source);                
                if (!eksport)
                    MessageBox.Show("Fejlet med at exportere.");
            }
            if (command == "import")
            {
                var pluginTask = Task.Run(async () => await api.ImportPlugin(source));
                pluginTask.Wait();
                var result = pluginTask.Result;
                if (!result)
                    MessageBox.Show("Fejlet med at importere.");
            }                    
            return ErrorCodes.Succes;
        }

        #region Uniconta Setup

        public string[] GetDependentAssembliesName()
        {
            return new string[] { };
        }

        public string GetErrorDescription()
        {
            return "";
        }

        public void Intialize()
        {

        }

        public void SetAPI(BaseAPI api)
        {
            _baseApi = api;
            _crudApi = new CrudAPI(_baseApi.session, _baseApi.CompanyEntity);
        }

        public void SetMaster(List<UnicontaBaseEntity> masters)
        {
        }

        #endregion

    }
}
