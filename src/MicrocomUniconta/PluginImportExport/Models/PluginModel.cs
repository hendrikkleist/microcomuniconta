﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicrocomUniconta.PluginImportExport.Models
{
    public class PluginModel
    {
        public string Name { get; set; }
        public string Control { get; set; }
        public string Dll { get; set; }
        public string Prompt { get; set; }
        public string ClassName { get; set; }
        public string Command { get; set; }
    }
}
