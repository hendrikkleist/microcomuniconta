; Microsoft Dynamics C5 Forms : TTPakkeFragt unloaded at Tirsdag 25.08.20
; --------------------------------------------------------------------------------
FRMVERSION 16

FORM TTPakkeFragt
    WINDOW    FIXED
    RUNMODE   LINK
    WIDTH     60
    HEIGHT    15
    VPWIDTH   0
    VPHEIGHT  0
    COLORSET  1
    FRAME     1
    POS       0 1
    RELPOS    273
    TITLEPOS  17
    TITLE     #Pakkelabels
    PROMPT    LEN

    LOCALS

        PROC 0 KEY 6400 NAME #Udskriv PakkeLabel
        ARG  #Glsfragt
    ENDLOCALS
    CQL  Init
        #STR 70 &Filename
        #STR 70 &Copyfile
        #STR 70 &copyfile2
        #STR 999 &converttxt
        #STR 10 &Ordrenr
        #STR 40 &Name
        #STR 40 &Adr1
        #STR 40 &Adr2
        #STR 10 &ZipCode
        #STR 30 &CityName
        #STR 40 &land
        #STR 40 &CountryCode
        #STR 40 &Email
        #Date   &Sentdate
        #Real   &Vaegt
        #STR 30 &Att
        #STR 10 &Kundenr
        #STR 30 &Mobil
        #STR 20 &Kartotek
        #
        #Set &Kartotek = &Parm
        #
        ##LocalMacro.Finddata
        #
        #    Set &Name=%1.Name
        #    Set &Adr1=%1.Address1
        #    If %1.ZipCity Then
        #      Set &Adr2=%1.Address2
        #      Set &ZipCode=Substr(%1.ZipCity,1,StrFind(%1.ZipCity," ",1,strlen(%1.ZipCity)))
        #      Set &CityName=Substr(%1.ZipCity,StrFind(%1.ZipCity," ",1,strlen(%1.ZipCity))+1,strlen(%1.ZipCity))
        #    Else
        #      Set &Adr2=""
        #      Set &ZipCode=Substr(%1.Address2,1,StrFind(%1.Address2," ",1,strlen(%1.Address2)))
        #      Set &CityName=Substr(%1.Address2,StrFind(%1.Address2," ",1,strlen(%1.Address2))+1,strlen(%1.Address2))
        #    Endif
        #    Set &CountryCode=Country[CodeIdx==%1.Country].IntrastatCode
        #    Set &land = ""
        #    Set &land = Country[codeidx==%1.Country].Country
        #    Set &att=%1.Attention
        #
        #  Set &Vaegt=1
        #  Set &pakkevaerdi=0
        #  Set &Kundenr=%1.Account
        #  Set &Sentdate=today()
        #
        ##EndMacro
        #
        #
    ENDCQL
    CQL  Exit
        #
        #Set &Converttxt='q:\\pakkelabels\\Convert\\Convert.bat'
        #
        #If &filename Then
        #  Process 1 'Mode=Winexec Command="'+&Converttxt+'"'
        #Endif
        #
        #
        #
        #
        #
    ENDCQL
    CQL  Pre-form
        #If &kartotek=="Salestable" Then
        #  Extern SalesTable
        #  #Finddata(Salestable)
        #  Set &Ordrenr = Salestable.Number
        #  Set &Mobil=Custtable[AccountIdx,Salestable.Account].CellPhone
        #  Set &email=Custtable[AccountIdx,SalesTable.InvoiceAccount].Email
        #  If Salestable.DlvAddress1 Then
        #    Set &Name=Salestable.DlvAddress1
        #    Set &Adr1=Salestable.DlvAddress2
        #    If Salestable.DlvAddress4 Then
        #      Set &Adr2=Salestable.DlvAddress3
        #      Set &ZipCode=Substr(Salestable.DlvAddress4,1,StrFind(Salestable.DlvAddress4," ",1,strlen(Salestable.DlvAddress4)))
        #      Set &CityName=Substr(Salestable.DlvAddress4,StrFind(Salestable.DlvAddress4," ",1,strlen(Salestable.DlvAddress4))+1,strlen(Salestable.DlvAddress4))
        #    Else
        #      Set &Adr2=""
        #      Set &ZipCode=Substr(Salestable.DlvAddress3,1,StrFind(Salestable.DlvAddress3," ",1,strlen(Salestable.DlvAddress3)))
        #      Set &CityName=Substr(Salestable.DlvAddress3,StrFind(Salestable.DlvAddress3," ",1,strlen(Salestable.DlvAddress3))+1,strlen(Salestable.DlvAddress3))
        #    Endif
        #    Set &CountryCode=Country[CodeIdx==Salestable.DlvCountry].IntrastatCode
        #    Set &land =""
        #    Set &land = Country[codeidx==Salestable.DlvCountry].Country
        #    if &land=="" Then
        #      Set &land = Country[codeidx==Salestable.Country].Country
        #      Set &CountryCode=Country[CodeIdx==Salestable.Country].IntrastatCode
        #    Endif
        #    Set &Att=Salestable.DlvAttention
        #  Endif
        #  Set &Vaegt=0
        #  Search Salesline Using NumTransLineIdx
        #  Where Salesline.Number==Salestable.Number
        #    Set &vaegt=&Vaegt+Inventable[ItemIdx,Salesline.ItemNumber].NetWeight * Salesline.Qty
        #  End
        #Endif
        #If &kartotek=="Vendtable" Then
        #  Extern Vendtable
        #  #Finddata(Vendtable)
        #  Set &Mobil=Vendtable.CellPhone
        #  Set &Ordrenr= Vendtable.Account
        #  Set &email=Vendtable.Email
        #Endif
        #If &Kartotek=="Custtable" Then
        #  Extern Custtable
        #  #Finddata(Custtable)
        #  Set &Mobil=Custtable.CellPhone
        #  Set &Ordrenr= Custtable.Account
        #  Set &email=Custtable.Email
        #Endif
        #
        #Set &filename = ""
        #Set &Copyfile2 = 'Q:\\Pakkelabels\\'+&Ordrenr+'.CSV'
        #
        #
        #
        #
        #
    ENDCQL


    BLOCK GLS
        FILE    -
        SEQNO   1
        RECORDS 1
        LINES   1
        MODE    ALL
        SQLMODE SELECTDEFAULT
        WARN    0
        AUTO    N
            WIDTH   0
            HEIGHT  0
            POS     0 0
            RECS    1 13
            FRAME   1
            COLOR   0
            MAXLEN  0
            FLAGS   COLLINE PROMPTS 
        ENDAUTO

        CQL  Pre-menu
            #STr 10 &STrPakkev
            #Str 70 &StrVaegt
            #STR 10 &Tomtekst
            #STR 10 &EmailsvarYN
            #Int    &talpak
            #Real    &Int1
            #Real    &Int2
            #Real    &int3
            #Real    &Int4
            #Real    &Int5
            #Real    &Int6
            #Real    &Int7
            #Real    &int8
            #Real    &int9
            #Real    &Int10
            #
            #
            #IF #InParm('Glsfragt')    THEN
            #
            #  Set &Tomtekst=""
            #
            #  SET &FileName = 'Q:\\Pakkelabels\\Convert\\Pakkelabels.CSV'
            #
            #  If Not &Adr1 Then
            #    Set Box(1,"Adresse 1 skal v�re udfyldt\n",0)
            #    Return 0
            #  Endif
            #
            #  If &Pakkevaerdi==0 then
            #    Set &strPakkev=''
            #  Endif
            #  If &Pakkevaerdi==1 Then
            #    Set &StrPakkev='27'
            #  Endif
            #  If &Pakkevaerdi==2 Then
            #    Set &StrPakkev='28'
            #  Endif
            #
            #  If &Att=='' Then
            #    Set &Att='.'
            #  Endif
            #
            #  Set &Ordrenr=Strrem(&ordrenr,'-')
            #
            #  Set &strvaegt=Num2str((&Vaegt*1000),1,0,1,0)
            #
            #  set &Kundenr="90195"
            #  Set &Mobil=""
            #
            #  SET &OUTRECDEL = #TextRecDel
            #  SET &OUTFLDDEL = ','
            #
            #
            #  write &filename As TEXT from (
            #    "shipping_agent,shipping_product_id,services,weight,receiver_name,receiver_address1,"+
            #    "receiver_zipcode,receiver_city,receiver_country,receiver_attention,receiver_email,"+
            #    "sender_name,sender_address1,sender_zipcode,sender_city,sender_country,printer_name,label_format,internal_id"
            #  )
            #
            #  write &filename AS TEXT FROM (
            #      '"gls"'
            #      ,"55"
            #      ,UserFormatStr(&Strpakkev,'',0,0)
            #      ,UserFormatStr(&Strvaegt,'',0,0)
            #      ,UserFormatStr(&Name,'"',0,0)
            #      ,UserFormatStr(&Adr1+" "+&Adr2,'"',0,0)
            #      ,UserFormatStr(&ZipCode,'"',0,0)
            #      ,UserFormatStr(&CityName,'"',0,0)
            #      ,'"DK"'
            #      ,UserFormatStr(&Att,'"',0,0)
            #      ,'""'
            #      ,'"ScanPOS ApS"'
            #      ,'"Langelandsvej 2"'
            #      ,'"9500"'
            #      ,'"Hobro"'
            #      ,'"DK"'
            #      ,'"ZDesigner GK420d"'
            #      ,'"zpl"'
            #      ,UserFormatStr(&Ordrenr,'',0,0)
            #
            #    )
            #  Control "Exit"
            #Endif
        ENDCQL

        FIELD Str 41 Name
            TYPE    VIRTUAL
            SEQNO   1
            PAGE    1
            POS     14 1
            LENGTH  40
            HEIGHT  1
            COLOR   5
            GROUPID 0
            TEXT    #Navn
            FLAGS   USER PROMPT ONELINE LEFT 
            SPECIALTYPE 0
        ENDFIELD
        FIELD Str 41 Adr1
            TYPE    VIRTUAL
            SEQNO   2
            PAGE    1
            POS     14 2
            LENGTH  40
            HEIGHT  1
            COLOR   5
            GROUPID 0
            TEXT    #Adresse1
            FLAGS   USER PROMPT ONELINE LEFT 
            SPECIALTYPE 0
        ENDFIELD
        FIELD Str 41 Adr2
            TYPE    VIRTUAL
            SEQNO   3
            PAGE    1
            POS     14 3
            LENGTH  40
            HEIGHT  1
            COLOR   5
            GROUPID 0
            TEXT    #Adresse2
            FLAGS   USER PROMPT ONELINE LEFT 
            SPECIALTYPE 0
        ENDFIELD
        FIELD Str 11 ZipCode
            TYPE    VIRTUAL
            SEQNO   4
            PAGE    1
            POS     14 4
            LENGTH  10
            HEIGHT  1
            COLOR   5
            GROUPID 0
            TEXT    #Postnr/By
            FLAGS   USER PROMPT ONELINE LEFT 
            SPECIALTYPE 0
        ENDFIELD
        FIELD Str 31 CityName
            TYPE    VIRTUAL
            SEQNO   5
            PAGE    1
            POS     25 4
            LENGTH  30
            HEIGHT  1
            COLOR   5
            GROUPID 0
            TEXT    #
            FLAGS   USER ONELINE LEFT 
            SPECIALTYPE 0
        ENDFIELD
        FIELD Date 0 Sentdate
            TYPE    VIRTUAL
            SEQNO   6
            PAGE    1
            POS     14 5
            LENGTH  10
            HEIGHT  1
            COLOR   5
            GROUPID 0
            TEXT    #Dato
            FLAGS   USER PROMPT ONELINE LEFT 
            SPECIALTYPE 0
        ENDFIELD
        FIELD Str 31 Att
            TYPE    VIRTUAL
            SEQNO   9
            PAGE    1
            POS     14 8
            LENGTH  30
            HEIGHT  1
            COLOR   5
            GROUPID 0
            TEXT    #Att
            FLAGS   USER PROMPT ONELINE LEFT 
            SPECIALTYPE 0
        ENDFIELD
        FIELD Real vaegt
            TYPE    VIRTUAL
            SEQNO   7
            PAGE    1
            POS     14 6
            LENGTH  10
            HEIGHT  1
            DEC     2
            POSOFS  0
            COLOR   5
            GROUPID 0
            TEXT    #V�gt
            FLAGS   USER PROMPT ONELINE RIGHT SIGNSTART DECCOMMA 
            SPECIALTYPE 0
            CQL  Post-Change
                #If &Vaegt<=0 Or &Vaegt>20 Then
                #  Return 0
                #Endif
            ENDCQL
        ENDFIELD
        FIELD Enum TTPakkev�rdi Pakkevaerdi
            TYPE    VIRTUAL
            SEQNO   8
            PAGE    1
            POS     14 7
            LENGTH  10
            HEIGHT  1
            COLOR   5
            GROUPID 0
            TEXT    #Pakkevaerdi
            FLAGS   USER PROMPT ONELINE LEFT 
            SPECIALTYPE 0
        ENDFIELD
        MENU #
            TITLE #
            FLAGS 2
            FRAME 1

                PROC 0
                NAME #Udskriv fragtbrev
                HELP #
                ARG  #Glsfragt
        ENDMENU
    ENDBLOCK

ENDFORM
